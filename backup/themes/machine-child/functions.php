<?php //nothing

//move wpautop filter to AFTER shortcode is processed
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop',100 );
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

add_shortcode('price', 'price_shortcode');
function price_shortcode($atts) {
    $atts = shortcode_atts(
		array(
			'text'			=> '',
			'price'			=> '',
			'sale_price'	=> '',
			'date'			=> '',
			'float'			=> ''
		),
        $atts
    );
	if ($atts['float']) {
		$style = 'style="float: '.$atts['float'].';"';
	}
	else {
		$style = '';
	}

	if ($atts['sale_price']) {
		return '
		<div class="price_bigger float_'.$atts['float'].'">
			<div class="text">' .$atts['text']. '</div>
			<div class="price_major">' .$atts['sale_price']. '</div>
			<div class="price_small">ord.pris: ' .$atts['price']. '</div>
			<div class="date">' .$atts['date']. '</div>
		</div>';
	}
	else {
		return '
		<div class="price_bigger float_'.$atts['float'].'">
			<div class="text"> &nbsp; </div>
			<div class="price_major">' .$atts['price']. '</div>
		</div>';
	}
}




add_shortcode('spa_overview_image_rea', 'spa_overview_image_rea_shortcode');
function spa_overview_image_rea_shortcode($atts) {
    $atts = shortcode_atts(
		array(
			'data'			=> ''
		),
        $atts
    );

	if ($atts['data']) {
		return '
		<div class="spa_overview_image_rea">
			<div class="data">' .$atts['data']. '</div>
		</div>';
	}
	else {
		return '';
	}
}

// [spa_overview_image_rea data="-35%"]


add_shortcode('specs_list', 'specs_list_shortcode');
function specs_list_shortcode($atts, $content) {
	$atts = shortcode_atts(
		array(
			'notused'		=> ''
		),
        $atts
    );
	return '
	<ul class="specs_list">
		' .do_shortcode($content). '
	</ul>
		';
}

add_shortcode('__spec', 'spec_item_shortcode');
function spec_item_shortcode($atts) {
    $atts = shortcode_atts(
		array(
			'title'			=> '',
			'data'			=> ''
		),
        $atts
    );
	$data = str_replace('%%br%%','<br>',$atts['data']);

	return '<li><div class="name_of_item">' .$atts['title']. '</div><div class="data">' .$data. '</div></li>';
}


/*
[specs_list]
[__spec title="Färger Skal" data="Vit Marmor, Granit"]
[__spec title="Färger Skal2" data="Vit Marmor, Granit"]
[__spec title="Färger Skal3" data="Vit Marmor, Granit"]
[__spec title="Färger Skal4" data="Vit Marmor, Granit"]
[__spec title="Färger Skal5" data="Vit Marmor, Granit"]
[__spec title="Färger Skal6" data="Vit Marmor, Granit"]
[__spec title="Färger Skal7" data="Vit Marmor, Granit"]
[/specs_list]
*/
