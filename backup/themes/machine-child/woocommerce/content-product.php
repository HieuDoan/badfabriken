<?php
global $product, $woocommerce_loop;

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;
?>

<div <?php post_class( 'col-md-4 col-sm-12' ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<div class="product">
		<div class="product-image">

			<?php
				if ( $product->is_on_sale() )
					echo apply_filters( 'woocommerce_sale_flash', '<span class="label">Rea!</span>', $post, $product );
			?>

			<div class="background-image-holder">
				<?php the_post_thumbnail('large'); ?>
			</div>

			<div class="hover-state">
				<div class="hover-content">
					<a href="<?php the_permalink(); ?>" class="btn btn-white btn-sm">Produktinfo</a>
					<?php
						/**
						 * woocommerce_after_shop_loop_item hook
						 *
						 * @hooked woocommerce_template_loop_add_to_cart - 10
						 */
						do_action( 'woocommerce_after_shop_loop_item' );
					?>
				</div>
			</div>
		</div>
		<?php
			the_title('<h5><a href="'. esc_url(get_permalink()) .'">', '</a></h5><br>');

			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>

	</div><!--end of inidividual product-->



</div>
