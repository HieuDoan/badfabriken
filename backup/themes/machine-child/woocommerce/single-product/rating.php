<?php
global $product;

if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if ( $rating_count > 0 ) : ?>
	
	<div class="reviews" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	
		<ul class="stars" title="<?php printf( __( 'Rated %s out of 5', 'machine' ), $average ); ?>">
			<?php echo $product->get_rating_html(); ?>
		</ul>
		
		<?php if ( comments_open() ) : ?>
		<a href="#reviews">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'machine' ), '<span itemprop="reviewCount" class="count">' . $review_count . '</span>' ); ?>)</a>
		<?php endif ?>
		
	</div>

<?php endif; ?>
