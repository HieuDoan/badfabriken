<?php
	global $post, $woocommerce, $product;

	$thumbnail = has_post_thumbnail();
?>

<div class="image-slider">
	<ul class="slides">

		<?php if ( $thumbnail ) : ?>
			<?php $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full'); ?>
			<li>
				<a href="<?php echo esc_url($url[0]); ?>" data-lightbox="true" data-title="<?php the_title(); ?>">
					<?php
						if ( $product->is_on_sale() )
							echo apply_filters( 'woocommerce_sale_flash', '<span class="label">' . __( 'Rea!', 'machine' ) . '</span>', $post, $product );
					?>
					<div class="background-image-holder">
						<?php the_post_thumbnail('large'); ?>
					</div>
				</a>
			</li>
		<?php endif; ?>

		<?php
			$attachment_ids = $product->get_gallery_attachment_ids();

			if ( $attachment_ids ) {
				foreach ( $attachment_ids as $attachment_id ) {
				$image = wp_get_attachment_image( $attachment_id, 'large' );
				$src = wp_get_attachment_image_src( $attachment_id, 'full' );
				?>

					<li>
						<a href="<?php echo esc_url($src[0]); ?>" data-lightbox="true" data-title="<?php the_title(); ?>">
							<div class="background-image-holder">
								<?php echo $image; ?>
							</div>
						</a>
					</li>

				<?php
				}
			}
		?>

	</ul>
</div>
