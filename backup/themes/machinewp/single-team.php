<?php
	get_header();
	the_post();
	
	$job = get_post_meta( $post->ID, '_ebor_the_job_title', true );
	$icons = get_post_meta( $post->ID, '_ebor_team_social_icons', true );
	
	ebor_the_page_title();
?>

<section class="article-wrapper">
	<div class="container">
		<div class="row">
		
			<div class="col-md-4 col-sm-4 text-center team-member team-2">
				<?php 
					the_post_thumbnail('full'); 
					echo '<span>'. esc_html($job) .'</span>';
				?>
			</div>

			<div class="col-md-8 col-sm-8">
				<div class="article-single">
				
					<div class="text-block">
						<div class="detail-line"></div>
						<?php the_title('<h4>', '</h4>'); ?>
					</div>
					
					<?php
						the_content();
						if( is_array($icons) ){
							echo '<ul class="social-links">';
								foreach( $icons as $key => $icon ){
									if(!( isset( $icon['_ebor_social_icon_url'] ) ))
										continue;
										
									echo '<li><a href="'. esc_url($icon['_ebor_social_icon_url']) .'" target="_blank"><i class="icon '. esc_attr($icon['_ebor_social_icon']) .'"></i></a></li>';
								}
							echo '</ul>';
						}
						wp_link_pages();
					?>
		
				</div>
			</div>

		</div><!--end of row-->
	</div><!--end of container-->
</section>		
						
<?php 
	get_footer();			