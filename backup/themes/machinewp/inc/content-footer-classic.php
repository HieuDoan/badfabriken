<footer class="footer-6">
	<div class="container">
	
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<?php
						if( is_active_sidebar('footer-sidebar') && !( is_active_sidebar('footer-sidebar-2') ) && !( is_active_sidebar('footer-sidebar-3') ) && !( is_active_sidebar('footer-sidebar-4') ) ){
							echo '<div class="col-sm-12">';
								dynamic_sidebar('footer-sidebar');
							echo '</div>';
						}
							
						if( is_active_sidebar('footer-sidebar-2') && !( is_active_sidebar('footer-sidebar-3') ) && !( is_active_sidebar('footer-sidebar-4') ) ){
							echo '<div class="col-sm-6">';
								dynamic_sidebar('footer-sidebar');
							echo '</div><div class="col-sm-6">';
								dynamic_sidebar('footer-sidebar-2');
							echo '</div><div class="clear"></div>';
						}
							
						if( is_active_sidebar('footer-sidebar-3') && !( is_active_sidebar('footer-sidebar-4') ) ){
							echo '<div class="col-sm-4">';
								dynamic_sidebar('footer-sidebar');
							echo '</div><div class="col-sm-4">';
								dynamic_sidebar('footer-sidebar-2');
							echo '</div><div class="col-sm-4">';
								dynamic_sidebar('footer-sidebar-3');
							echo '</div><div class="clear"></div>';
						}
						
						if( is_active_sidebar('footer-sidebar-4') ){
							echo '<div class="col-sm-3">';
								dynamic_sidebar('footer-sidebar');
							echo '</div><div class="col-sm-3">';
								dynamic_sidebar('footer-sidebar-2');
							echo '</div><div class="col-sm-3">';
								dynamic_sidebar('footer-sidebar-3');
							echo '</div><div class="col-sm-3">';
								dynamic_sidebar('footer-sidebar-4');
							echo '</div><div class="clear"></div>';
						}
					?>
				</div>
			</div>
		</div><!--end of row-->
	
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-lower">
				
					<span><?php echo get_option('copyright','<a href="http://www.tommusrhodus.com">Machine Premium WordPress Theme by TommusRhodus</a>'); ?></span>
					
					<ul class="social-links">
						<?php
							$protocols = array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet', 'skype');
							
							for( $i = 1; $i < 7; $i++ ){
								if( get_option("footer_social_url_$i") ) {
									echo '<li>
										      <a href="' . esc_url(get_option("footer_social_url_$i"), $protocols) . '" target="_blank">
											      <i class="icon ' . get_option("footer_social_icon_$i") . '"></i>
										      </a>
										  </li>';
								}
							} 
						?>
					</ul>
					
				</div>
			</div>
		</div><!--end of row-->
	
	</div><!--end of container-->	
</footer>