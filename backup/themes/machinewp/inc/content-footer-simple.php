<?php $logo = get_option('custom_logo_dark', EBOR_THEME_DIRECTORY . 'style/img/logo-square-dark.png'); ?>

<footer class="footer-4">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<ul class="social-links">
					<?php
						/**
						 * Subfooter nav menu, allows top level items only
						 */
						if ( has_nav_menu( 'footer' ) ) { 
						    wp_nav_menu( 
							    array(
							        'theme_location'    => 'footer',
							        'depth'             => 1,
							        'container'         => false,
							        'container_class'   => false,
							        'menu_class'        => false,
							        'items_wrap'        => '%3$s'
							    ) 
						    );
						}
					?>
				</ul>
			</div>
		</div><!--end of row-->

		<div class="row">
			<div class="col-sm-12 text-center">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<?php if( $logo ) : ?>
						<img alt="<?php esc_attr(bloginfo('name')); ?>" class="logo" src="<?php echo esc_url($logo); ?>" />
					<?php else : ?>
						<h5 class="text-logo dark font4"><?php get_bloginfo('name'); ?></h5>
					<?php endif; ?>
				</a>
				<span><?php echo get_option('copyright','<a href="http://www.tommusrhodus.com">Machine Premium WordPress Theme by TommusRhodus</a>'); ?></span>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->	
</footer>