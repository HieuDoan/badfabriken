<?php $logo = get_option('custom_logo_dark', EBOR_THEME_DIRECTORY . 'style/img/logo-square-dark.png'); ?>

<footer class="footer-2">
	<div class="container">
	
		<div class="row">
			<div class="col-sm-6">
				<a href="<?php echo esc_url(home_url('/')); ?>" class="pull-left">
					<?php if( $logo ) : ?>
						<img alt="<?php esc_attr(bloginfo('name')); ?>" class="logo" src="<?php echo esc_url($logo); ?>" />
					<?php else : ?>
						<h5 class="text-logo dark font4"><?php echo get_bloginfo('name'); ?></h5>
					<?php endif; ?>
				</a>
				<span class="tagline"><?php bloginfo('description'); ?></span>
			</div>
		
			<div class="col-sm-6 text-right">
				<ul class="social-links">
					<?php
						$protocols = array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet', 'skype');
						
						for( $i = 1; $i < 4; $i++ ){
							if( get_option("footer_social_url_$i") ) {
								echo '<li>
									      <a href="' . esc_url(get_option("footer_social_url_$i"), $protocols) . '" target="_blank">
										      <i class="icon ' . get_option("footer_social_icon_$i") . '"></i>
									      </a>
									  </li>';
							}
						} 
					?>
				</ul>
			</div>
		</div><!--end of row-->
	
		<div class="row">
			<div class="col-xs-12">
				<div class="footer-lower">
					<span class="copyright"><?php echo get_option('copyright','<a href="http://www.tommusrhodus.com">Machine Premium WordPress Theme by TommusRhodus</a>'); ?></span>
				</div>
			</div>
		</div><!--end of row-->
		
	</div><!--end of container-->
</footer>