<?php $logo = get_option('custom_logo_dark', EBOR_THEME_DIRECTORY . 'style/img/logo-square-dark.png'); ?>

<nav class="nav-2 nav-light">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			
				<a href="<?php echo esc_url(home_url('/')); ?>" class="home-link">
					<?php if( $logo ) : ?>
						<img alt="<?php esc_attr(bloginfo('name')); ?>" class="logo" src="<?php echo esc_url($logo); ?>" />
					<?php else : ?>
						<h5 class="text-logo dark font4"><?php get_bloginfo('name'); ?></h5>
					<?php endif; ?>
				</a>
				
				<div class="text-right">
				
					<ul class="social-links">
						<?php
							if(function_exists('ebor_cart_icon'))
								ebor_cart_icon();
								
							$protocols = array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet', 'skype');
							
							for( $i = 1; $i < 4; $i++ ){
								if( get_option("header_social_url_$i") ) {
									echo '<li>
										      <a href="' . esc_url(get_option("header_social_url_$i"), $protocols) . '" target="_blank">
											      <i class="icon ' . get_option("header_social_icon_$i") . '"></i>
										      </a>
										  </li>';
								}
							} 
							
							if( function_exists('icl_get_languages') )
								ebor_language_selector_flags();
						?>
					</ul>
					
					<div class="menu-toggle">
						<div class="bar-1"></div>
						<div class="bar-2"></div>
						<div class="bar-3"></div>
					</div>
			
					<?php
						if ( has_nav_menu( 'primary' ) ){
						    wp_nav_menu( 
						    	array(
							        'theme_location'    => 'primary',
							        'depth'             => 1,
							        'container'         => false,
							        'container_class'   => false,
							        'menu_class'        => 'menu',
							        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							        'walker'            => new ebor_framework_medium_rare_bootstrap_navwalker()
						        )
						    );  
						} else {
							echo '<ul class="menu"><li><a href="'. admin_url('nav-menus.php') .'">Set up a navigation menu now</a></li></ul>';
						}
					?>
					
				</div>
				
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</nav>