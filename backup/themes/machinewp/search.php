<?php 
	get_header(); 
	
	global $wp_query;
	$total_results = $wp_query->found_posts;
	$items = ( $total_results == '1' ) ? __(' item','machine') : __(' items','machine');
	$background = get_option('blog_header');
	
	echo ebor_archive_header( get_search_query() . __(', found ' ,'machine') . $total_results . $items, $background );
	
	get_template_part('loop/loop', 'blog-classic-sidebar');
	get_footer();				