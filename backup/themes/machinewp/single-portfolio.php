<?php
	get_header();
	the_post();
	
	ebor_the_page_title();
?>

<section class="article-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="article-single">
					<?php
						the_content();
						wp_link_pages();
					?>
				</div>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</section>		
						
<?php 
	get_footer();			