<?php 
	/**
	 * comments.php
	 * The comments template used in loom
	 * @author TommusRhodus
	 * @package loom
	 * @since 1.0.0
	 */
	$custom_comment_form = array( 
		'fields' => apply_filters( 'comment_form_default_fields', array(
		    'author' => '<input type="text" class="input-standard validate-required" id="author" name="author" placeholder="' . __('Name *','machine') . '" value="' . esc_attr( $commenter['comment_author'] ) . '" />',
		    'email'  => '<input name="email" class="input-standard validate-required" type="text" id="email" placeholder="' . __('Email *','machine') . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" />',
		    'url'    => '<input name="url" class="input-standard validate-required" type="text" id="url" placeholder="' . __('Website','machine') . '" value="' . esc_attr(  $commenter['comment_author_url'] ) . '" />') 
		),
		'comment_field' => '<textarea class="input-standard validate-required" name="comment" placeholder="' . __('Your Comment Here','machine') . '" id="comment" aria-required="true" rows="8"></textarea>',
		'cancel_reply_link' => __( 'Cancel' , 'machine' ),
		'comment_notes_before' => '',
		'comment_notes_after' => '',
		'label_submit' => __( 'Submit' , 'machine' )
	);
?>

<section class="blog-comments">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="comments-list">
				
					<h5><?php comments_number( __('0 Comments','machine'), __('1 Comment','machine'), __('% Comments','machine') ); ?></h5>
					
					<?php 
						if( have_comments() ){
						  echo '<ol id="singlecomments" class="commentlist">';
						  wp_list_comments('type=comment&callback=ebor_custom_comment');
						  echo '</ol>';
						}
						paginate_comments_links(); 
					?>
					
					<?php comment_form($custom_comment_form); ?>
					
				</div>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</section>