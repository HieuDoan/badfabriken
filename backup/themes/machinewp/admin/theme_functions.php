<?php 

/**
 * WordPress' missing is_blog_page() function.  Determines if the currently viewed page is
 * one of the blog pages, including the blog home page, archive, category/tag, author, or single
 * post pages.
 *
 * @see http://core.trac.wordpress.org/browser/tags/3.4.1/wp-includes/query.php#L1572
 *
 * @return bool
 */
if(!( function_exists('is_blog_page') )){
	function is_blog_page() {
	    global $post;
	    return ( ( is_home() || is_archive() || is_single() ) && ('post' == get_post_type($post)) ) ? true : false ;
	}
}

if(!( function_exists('ebor_get_header_options') )){
	function ebor_get_header_options(){
		$options = array(
			'blank' => 'No Header or Nav',
			'bar' => 'Bar Header',
			'bar-light' => 'Bar Light Header',
			'burger' => 'Burger Menu Header',
			'burger-light' => 'Burger Menu Light Header'
		);
		return $options;	
	}
}

if(!( function_exists('ebor_get_footer_options') )){
	function ebor_get_footer_options(){
		$options = array(
			'blank' => 'No Footer',
			'agency' => 'Agency Footer',
			'classic' => 'Classic Widgets Widgets',
			'menu' => 'Menu Footer',
			'simple' => 'Simple Footer',
			'social' => 'Social Footer'
		);
		return $options;	
	}
}

/**
 * ebor_get_footer_layout
 * 
 * Use to conditionally check the page footer meta layout against the theme option for the same
 * In short, this function can override the global footer option on a post by post basis
 * Call within get_footer() for this to override the global footer choice
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_get_footer_layout') )){
	function ebor_get_footer_layout(){
		global $post;
		
		if(!( isset($post->ID) ))
			return get_option('footer_layout', 'classic');
			
		$footer = get_post_meta($post->ID, '_ebor_footer_override', 1);
		if( '' == $footer || false == $footer || 'none' == $footer ){
			$footer = get_option('footer_layout', 'classic');
		}
		return $footer;	
	}
}

/**
 * ebor_get_header_layout
 * 
 * Use to conditionally check the page header meta layout against the theme option for the same
 * In short, this function can override the global header option on a post by post basis
 * Call within get_header() for this to override the global header choice
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_get_header_layout') )){
	function ebor_get_header_layout(){
		global $post;
		
		if(!( isset($post->ID) ))
			return get_option('header_layout', 'bar');
		
		$header = get_post_meta($post->ID, '_ebor_header_override', 1);
		if( '' == $header || false == $header || 'none' == $header ){
			$header = get_option('header_layout', 'bar');
		}
		
		return $header;	
	}
}

/**
 * Collect and display post subtitle
 */
if(!( function_exists('ebor_the_subtitle') )){
	function ebor_the_subtitle($before = false, $after = false){
		
		global $post;
		$subtitle = get_post_meta( $post->ID, '_ebor_the_subtitle', true );
		
		if( isset($post) && $subtitle  ){
			echo $before . $subtitle . $after;
		} else {
			return false;
		}
		
	}
}

if(!( function_exists('ebor_archive_header') )){
	function ebor_archive_header($title = false, $image = false){
		$output = '<section class="hero-slider large-image short-header fixed-header">
			<ul class="slides">
				<li>
				
					<div class="background-image-holder background-parallax">
						<img alt="Slide Background" class="background-image" src="'. esc_url($image) .'">
					</div>
		
					<div class="container vertical-align">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h1 class="text-white">'. $title .'</h1>
							</div>
						</div>
					</div><!--end of container-->	
					
				</li>
			</ul>
		</section>';
		
		return $output;
	}
}

if(!( function_exists('ebor_get_portfolio_layouts') )){
	function ebor_get_portfolio_layouts(){
		return array_flip(array(
			'Featured Portfolio' => 'featured',
			'Grid Portfolio' => 'grid',
			'Large Portfolio' => 'large'
		));	
	}
}

/**
 * HEX to RGB Converter
 *
 * Converts a HEX input to an RGB array.
 * @param $hex - the inputted HEX code, can be full or shorthand, #ffffff or #fff
 * @since 1.0.0
 * @return string
 * @author tommusrhodus
 */
if(!( function_exists('ebor_hex2rgb') )){
	function ebor_hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);
	
	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   return implode(",", $rgb); // returns the rgb values separated by commas
	   return $rgb; // returns an array with the rgb values
	}
}

/**
 * Portfolio Unlimited
 * Uses pre_get_posts to provide unlimited portfolio posts when viewing the /portfolio/ archive
 * @since 1.0.0
 */
if(!(function_exists( 'ebor_portfolio_unlimited' ))){
	function ebor_portfolio_unlimited( $query ) {
	    if ( 
	    	is_post_type_archive('portfolio') && !( is_admin() ) && $query->is_main_query() ||
	    	is_tax('portfolio_category') && !( is_admin() ) && $query->is_main_query()
	    ) {
	        $query->set( 'posts_per_page', '-1' );
	    }    
	    return;
	}
	add_action( 'pre_get_posts', 'ebor_portfolio_unlimited' );
}

/**
 * Init theme options
 * Certain theme options need to be written to the database as soon as the theme is installed.
 * This is either for the enqueues in ebor-framework, or to override the default image sizes in WooCommerce.
 * Either way this function is only called when the theme is first activated, de-activating and re-activating the theme will result in these options returning to defaults.
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_init_theme_options') )){
	/**
	 * Hook in on activation
	 */
	global $pagenow;
	
	/**
	 * Define image sizes
	 */
	function ebor_init_theme_options() {
	  	$catalog = array(
			'width' 	=> '440',	// px
			'height'	=> '295',	// px
			'crop'		=> 1 		// true
		);
	
		$single = array(
			'width' 	=> '600',	// px
			'height'	=> '600',	// px
			'crop'		=> 1 		// true
		);
	
		$thumbnail = array(
			'width' 	=> '113',	// px
			'height'	=> '113',	// px
			'crop'		=> 1 		// false
		);
	
		// Image sizes
		update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
		update_option( 'shop_single_image_size', $single ); 		// Single product image
		update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
		
		//Ebor Framework
		$framework_args = array(
			'portfolio_post_type'   => '1',
			'team_post_type'        => '1',
			'client_post_type'      => '1',
			'testimonial_post_type' => '1',
			'mega_menu'             => '1',
			'aq_resizer'            => '0',
			'page_builder'          => '0',
			'likes'                 => '0',
			'options'               => '1',
			'metaboxes'             => '1',
			'pivot_shortcodes'      => '1',
			'machine_widgets'       => '1'
		);
		update_option('ebor_framework_options', $framework_args);
	}
	
	/**
	 * Only call this action when we first activate the theme.
	 */
	if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' || is_admin() && isset( $_GET['theme'] ) && $pagenow == 'customize.php' ){
		add_action( 'init', 'ebor_init_theme_options', 1 );
	}
}



/**
 * Register the required plugins for this theme.
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_register_required_plugins') )){
	function ebor_register_required_plugins() {
		$plugins = array(
			array(
			    'name'      => 'Contact Form 7',
			    'slug'      => 'contact-form-7',
			    'required'  => false,
			    'version' 	=> '3.7.2'
			),
			array(
			    'name'      => 'WooCommerce',
			    'slug'      => 'woocommerce',
			    'required'  => false,
			    'version' 	=> '2.0.0'
			),
			array(
				'name'     				=> 'Ebor Framework',
				'slug'     				=> 'Ebor-Framework-master',
				'source'   				=> 'https://github.com/tommusrhodus/ebor-framework/archive/master.zip',
				'required' 				=> true,
				'version' 				=> '1.0.0',
				'external_url' 			=> 'https://github.com/tommusrhodus/ebor-framework/archive/master.zip',
			),
			array(
				'name'     				=> 'Visual Composer',
				'slug'     				=> 'js_composer',
				'source'   				=> 'http://www.madeinebor.com/plugin-downloads/js_composer-latest.zip',
				'required' 				=> true,
				'external_url' 			=> 'http://www.madeinebor.com/plugin-downloads/js_composer-latest.zip',
				'version' 				=> '5.1.1',
			),
		);
		$config = array(
			'is_automatic' => true,
		);
		tgmpa( $plugins, $config );
	}
	add_action( 'tgmpa_register', 'ebor_register_required_plugins' );
}

if(!( function_exists('ebor_pagination') )){
	function ebor_pagination($pages = '', $range = 2){
		$showitems = ($range * 2)+1;
		
		global $paged;
		if(empty($paged)) $paged = 1;
		
		if($pages == ''){
			global $wp_query;
			$pages = $wp_query->max_num_pages;
				if(!$pages) {
					$pages = 1;
				}
		}
		
		$output = '';
		
		if(1 != $pages){
			
		if(1 != $pages){
			$output .= "<div class='row'><div class='col-sm-12 text-center'><ul class='pagination'>";
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link(1)."'><span aria-hidden='true'>«</span><span class='sr-only'>Previous</span></a></li> ";
			
			for ($i=1; $i <= $pages; $i++){
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
					$output .= ($paged == $i)? "<li class='active'><a href='".get_pagenum_link($i)."'>".$i."</a></li> ":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li> ";
				}
			}
		
			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link($pages)."'><span aria-hidden='true'>»</span><span class='sr-only'>Next</span></a></li> ";
			$output.= "</ul></div></div>";
		}
		
		}
		
		return $output;
	}
}

if(!( function_exists('ebor_portfolio_filters') )){
	function ebor_portfolio_filters($cats){
		
		$output = '<div class="row"><div class="col-sm-12"><ul class="filters"><li data-filter="all" class="active">'. __('All','machine') .'</li>';
		if(is_array($cats)){
			foreach($cats as $cat){
				$output .= '<li data-filter="'. esc_attr($cat->slug) .'">'. $cat->name .'</li>';
			}
		}
		$output .= '</ul></div></div>';
		
		return $output;	
	}
}

/**
 * Add additional styling options to TinyMCE
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_mce_buttons_2') )){
	function ebor_mce_buttons_2( $buttons ) {
	    array_unshift( $buttons, 'styleselect' );
	    return $buttons;
	}
	add_filter( 'mce_buttons_2', 'ebor_mce_buttons_2' );
}

/**
 * Add additional styling options to TinyMCE
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_mce_before_init') )){
	function ebor_mce_before_init( $settings ) {
	    $style_formats = array(
	    	array(
	    		'title' => 'Huge H1',
	    		'selector' => 'h1',
	    		'classes' => 'jumbo-h1',
	    	),
	    	array(
	    		'title' => 'Hero H1',
	    		'selector' => 'h1',
	    		'classes' => 'text-hero',
	    	),
	    	array(
	    		'title' => 'Washed Out H1',
	    		'selector' => 'h1',
	    		'classes' => 'contrast-h1',
	    	),
	    	array(
	    		'title' => 'Highlight H2',
	    		'selector' => 'h2',
	    		'classes' => 'blue',
	    	),
	    	array(
	    		'title' => 'Chunky H3',
	    		'selector' => 'h3',
	    		'classes' => 'alt-font uppercase',
	    	),
	    	array(
	    		'title' => 'Super Lead P',
	    		'selector' => 'p',
	    		'classes' => 'super-lead',
	    	),
	    	array(
	    		'title' => 'Lead P',
	    		'selector' => 'p',
	    		'classes' => 'lead',
	    	),
	    	array(
	    		'title' => 'Uppercase P',
	    		'selector' => 'p',
	    		'classes' => 'uppercase',
	    	),
	    	array(
	    		'title' => 'Fancy Link',
	    		'selector' => 'a',
	    		'classes' => 'text-link',
	    	),
	    	array(
	    		'title' => 'Button',
	    		'selector' => 'a',
	    		'classes' => 'btn',
	    	),
	    	array(
	    		'title' => 'White Button',
	    		'selector' => 'a',
	    		'classes' => 'btn btn-white',
	    	),
	    	array(
	    		'title' => 'Filled Button',
	    		'selector' => 'a',
	    		'classes' => 'btn btn-filled',
	    	),
	    	array(
	    		'title' => 'Arrow List',
	    		'selector' => 'ul',
	    		'classes' => 'arrow-list',
	    	),
	    );
	    $settings['style_formats'] = json_encode( $style_formats );
	    return $settings;
	}
	add_filter( 'tiny_mce_before_init', 'ebor_mce_before_init' );
}

if(!( function_exists('ebor_get_social_icons') )){
	function ebor_get_social_icons(){
		$icons = array(
			"none" => "None",
			"social_facebook" => "facebook",
			"social_twitter" => "twitter",
			"social_pinterest" => "pinterest",
			"social_googleplus" => "googleplus",
			"social_tumblr" => "tumblr",
			"social_tumbleupon" => "tumbleupon",
			"social_wordpress" => "wordpress",
			"social_instagram" => "instagram",
			"social_dribbble" => "dribbble",
			"social_vimeo" => "vimeo",
			"social_linkedin" => "linkedin",
			"social_rss" => "rss",
			"social_deviantart" => "deviantart",
			"social_share" => "share",
			"social_myspace" => "myspace",
			"social_skype" => "skype",
			"social_youtube" => "youtube",
			"social_picassa" => "picassa",
			"social_googledrive" => "googledrive",
			"social_flickr" => "flickr",
			"social_blogger" => "blogger",
			"social_spotify" => "spotify",
			"social_delicious" => "delicious",
			"social_facebook_circle" => "facebook_circle",
			"social_twitter_circle" => "twitter_circle",
			"social_pinterest_circle" => "pinterest_circle",
			"social_googleplus_circle" => "googleplus_circle",
			"social_tumblr_circle" => "tumblr_circle",
			"social_stumbleupon_circle" => "stumbleupon_circle",
			"social_wordpress_circle" => "wordpress_circle",
			"social_instagram_circle" => "instagram_circle",
			"social_dribbble_circle" => "dribbble_circle",
			"social_vimeo_circle" => "vimeo_circle",
			"social_linkedin_circle" => "linkedin_circle",
			"social_rss_circle" => "rss_circle",
			"social_deviantart_circle" => "deviantart_circle",
			"social_share_circle" => "share_circle",
			"social_myspace_circle" => "myspace_circle",
			"social_skype_circle" => "skype_circle",
			"social_youtube_circle" => "youtube_circle",
			"social_picassa_circle" => "picassa_circle",
			"social_googledrive_alt2" => "googledrive_alt2",
			"social_flickr_circle" => "flickr_circle",
			"social_blogger_circle" => "blogger_circle",
			"social_spotify_circle" => "spotify_circle",
			"social_delicious_circle" => "delicious_circle",
			"social_facebook_square" => "facebook_square",
			"social_twitter_square" => "twitter_square",
			"social_pinterest_square" => "pinterest_square",
			"social_googleplus_square" => "googleplus_square",
			"social_tumblr_square" => "tumblr_square",
			"social_stumbleupon_square" => "stumbleupon_square",
			"social_wordpress_square" => "wordpress_square",
			"social_instagram_square" => "instagram_square",
			"social_dribbble_square" => "dribbble_square",
			"social_vimeo_square" => "vimeo_square",
			"social_linkedin_square" => "linkedin_square",
			"social_rss_square" => "rss_square",
			"social_deviantart_square" => "deviantart_square",
			"social_share_square" => "share_square",
			"social_myspace_square" => "myspace_square",
			"social_skype_square" => "skype_square",
			"social_youtube_square" => "youtube_square",
			"social_picassa_square" => "picassa_square",
			"social_googledrive_square" => "googledrive_square",
			"social_flickr_square" => "flickr_square",
			"social_blogger_square" => "blogger_square",
			"social_spotify_square" => "spotify_square",
			"social_delicious_square" => "delicious_square",
		);
		return $icons;
	}	
}

if(!( function_exists('ebor_get_icons') )){
	function ebor_get_icons(){
		$icons = array(
			'none',
			'pe-7s-cloud-upload',
			'pe-7s-cash',
			'pe-7s-close',
			'pe-7s-bluetooth',
			'pe-7s-cloud-download',
			'pe-7s-way',
			'pe-7s-close-circle',
			'pe-7s-id',
			'pe-7s-angle-up',
			'pe-7s-wristwatch',
			'pe-7s-angle-up-circle',
			'pe-7s-world',
			'pe-7s-angle-right',
			'pe-7s-volume',
			'pe-7s-angle-right-circle',
			'pe-7s-users',
			'pe-7s-angle-left',
			'pe-7s-user-female',
			'pe-7s-angle-left-circle',
			'pe-7s-up-arrow',
			'pe-7s-angle-down',
			'pe-7s-switch',
			'pe-7s-angle-down-circle',
			'pe-7s-scissors',
			'pe-7s-wallet',
			'pe-7s-safe',
			'pe-7s-volume2',
			'pe-7s-volume1',
			'pe-7s-voicemail',
			'pe-7s-video',
			'pe-7s-user',
			'pe-7s-upload',
			'pe-7s-unlock',
			'pe-7s-umbrella',
			'pe-7s-trash',
			'pe-7s-tools',
			'pe-7s-timer',
			'pe-7s-ticket',
			'pe-7s-target',
			'pe-7s-sun',
			'pe-7s-study',
			'pe-7s-stopwatch',
			'pe-7s-star',
			'pe-7s-speaker',
			'pe-7s-signal',
			'pe-7s-shuffle',
			'pe-7s-shopbag',
			'pe-7s-share',
			'pe-7s-server',
			'pe-7s-search',
			'pe-7s-film',
			'pe-7s-science',
			'pe-7s-disk',
			'pe-7s-ribbon',
			'pe-7s-repeat',
			'pe-7s-refresh',
			'pe-7s-add-user',
			'pe-7s-refresh-cloud',
			'pe-7s-paperclip',
			'pe-7s-radio',
			'pe-7s-note2',
			'pe-7s-print',
			'pe-7s-network',
			'pe-7s-prev',
			'pe-7s-mute',
			'pe-7s-power',
			'pe-7s-medal',
			'pe-7s-portfolio',
			'pe-7s-like2',
			'pe-7s-plus',
			'pe-7s-left-arrow',
			'pe-7s-play',
			'pe-7s-key',
			'pe-7s-plane',
			'pe-7s-joy',
			'pe-7s-photo-gallery',
			'pe-7s-pin',
			'pe-7s-phone',
			'pe-7s-plug',
			'pe-7s-pen',
			'pe-7s-right-arrow',
			'pe-7s-paper-plane',
			'pe-7s-delete-user',
			'pe-7s-paint',
			'pe-7s-bottom-arrow',
			'pe-7s-notebook',
			'pe-7s-note',
			'pe-7s-next',
			'pe-7s-news-paper',
			'pe-7s-musiclist',
			'pe-7s-music',
			'pe-7s-mouse',
			'pe-7s-more',
			'pe-7s-moon',
			'pe-7s-monitor',
			'pe-7s-micro',
			'pe-7s-menu',
			'pe-7s-map',
			'pe-7s-map-marker',
			'pe-7s-mail',
			'pe-7s-mail-open',
			'pe-7s-mail-open-file',
			'pe-7s-magnet',
			'pe-7s-loop',
			'pe-7s-look',
			'pe-7s-lock',
			'pe-7s-lintern',
			'pe-7s-link',
			'pe-7s-like',
			'pe-7s-light',
			'pe-7s-less',
			'pe-7s-keypad',
			'pe-7s-junk',
			'pe-7s-info',
			'pe-7s-home',
			'pe-7s-help2',
			'pe-7s-help1',
			'pe-7s-graph3',
			'pe-7s-graph2',
			'pe-7s-graph1',
			'pe-7s-graph',
			'pe-7s-global',
			'pe-7s-gleam',
			'pe-7s-glasses',
			'pe-7s-gift',
			'pe-7s-folder',
			'pe-7s-flag',
			'pe-7s-filter',
			'pe-7s-file',
			'pe-7s-expand1',
			'pe-7s-exapnd2',
			'pe-7s-edit',
			'pe-7s-drop',
			'pe-7s-drawer',
			'pe-7s-download',
			'pe-7s-display2',
			'pe-7s-display1',
			'pe-7s-diskette',
			'pe-7s-date',
			'pe-7s-cup',
			'pe-7s-culture',
			'pe-7s-crop',
			'pe-7s-credit',
			'pe-7s-copy-file',
			'pe-7s-config',
			'pe-7s-compass',
			'pe-7s-comment',
			'pe-7s-coffee',
			'pe-7s-cloud',
			'pe-7s-clock',
			'pe-7s-check',
			'pe-7s-chat',
			'pe-7s-cart',
			'pe-7s-camera',
			'pe-7s-call',
			'pe-7s-calculator',
			'pe-7s-browser',
			'pe-7s-box2',
			'pe-7s-box1',
			'pe-7s-bookmarks',
			'pe-7s-bicycle',
			'pe-7s-bell',
			'pe-7s-battery',
			'pe-7s-ball',
			'pe-7s-back',
			'pe-7s-attention',
			'pe-7s-anchor',
			'pe-7s-albums',
			'pe-7s-alarm',
			'pe-7s-airplay',
			'arrow_back', 
			'arrow_carrot_up_alt', 
			'arrow_carrot-2down_alt2', 
			'arrow_carrot-2down', 
			'arrow_carrot-2dwnn_alt', 
			'arrow_carrot-2left_alt', 
			'arrow_carrot-2left_alt2', 
			'arrow_carrot-2left', 
			'arrow_carrot-2right_alt', 
			'arrow_carrot-2right_alt2', 
			'arrow_carrot-2right', 
			'arrow_carrot-2up_alt', 
			'arrow_carrot-2up_alt2', 
			'arrow_carrot-2up', 
			'arrow_carrot-down_alt', 
			'arrow_carrot-down_alt2', 
			'arrow_carrot-down', 
			'arrow_carrot-left_alt', 
			'arrow_carrot-left_alt2', 
			'arrow_carrot-left', 
			'arrow_carrot-right_alt', 
			'arrow_carrot-right_alt2', 
			'arrow_carrot-right', 
			'arrow_carrot-up_alt2', 
			'arrow_carrot-up', 
			'arrow_condense_alt', 
			'arrow_condense', 
			'arrow_down_alt', 
			'arrow_down', 
			'arrow_expand_alt', 
			'arrow_expand_alt2', 
			'arrow_expand_alt3', 
			'arrow_expand', 
			'arrow_left_alt', 
			'arrow_left-down_alt', 
			'arrow_left-down', 
			'arrow_left-right_alt', 
			'arrow_left-right', 
			'arrow_left-up_alt', 
			'arrow_left-up', 
			'arrow_left', 
			'arrow_move', 
			'arrow_right_alt', 
			'arrow_right-down_alt', 
			'arrow_right-down', 
			'arrow_right-up_alt', 
			'arrow_right-up', 
			'arrow_right', 
			'arrow_triangle-down_alt', 
			'arrow_triangle-down_alt2', 
			'arrow_triangle-down', 
			'arrow_triangle-left_alt', 
			'arrow_triangle-left_alt2', 
			'arrow_triangle-left', 
			'arrow_triangle-right_alt', 
			'arrow_triangle-right_alt2', 
			'arrow_triangle-right', 
			'arrow_triangle-up_alt', 
			'arrow_triangle-up_alt2', 
			'arrow_triangle-up', 
			'arrow_up_alt', 
			'arrow_up-down_alt', 
			'arrow_up', 
			'arrow-up-down', 
			'icon_adjust-horiz', 
			'icon_adjust-vert', 
			'icon_archive_alt', 
			'icon_archive', 
			'icon_bag_alt', 
			'icon_bag', 
			'icon_balance', 
			'icon_blocked', 
			'icon_book_alt', 
			'icon_book', 
			'icon_box-checked', 
			'icon_box-empty', 
			'icon_box-selected', 
			'icon_briefcase_alt', 
			'icon_briefcase', 
			'icon_building_alt', 
			'icon_building', 
			'icon_calculator_alt', 
			'icon_calendar', 
			'icon_calulator', 
			'icon_camera_alt', 
			'icon_camera', 
			'icon_cart_alt', 
			'icon_cart', 
			'icon_chat_alt', 
			'icon_chat', 
			'icon_check_alt', 
			'icon_check_alt2', 
			'icon_check', 
			'icon_circle-empty', 
			'icon_circle-slelected', 
			'icon_clipboard', 
			'icon_clock_alt', 
			'icon_clock', 
			'icon_close_alt', 
			'icon_close_alt2', 
			'icon_close', 
			'icon_cloud_alt', 
			'icon_cloud-download_alt', 
			'icon_cloud-download', 
			'icon_cloud-upload_alt', 
			'icon_cloud-upload', 
			'icon_cloud', 
			'icon_cog', 
			'icon_cogs', 
			'icon_comment_alt', 
			'icon_comment', 
			'icon_compass_alt', 
			'icon_compass', 
			'icon_cone_alt', 
			'icon_cone', 
			'icon_contacts_alt', 
			'icon_contacts', 
			'icon_creditcard', 
			'icon_currency_alt', 
			'icon_currency', 
			'icon_cursor_alt', 
			'icon_cursor', 
			'icon_datareport_alt', 
			'icon_datareport', 
			'icon_desktop', 
			'icon_dislike_alt', 
			'icon_dislike', 
			'icon_document_alt', 
			'icon_document', 
			'icon_documents_alt', 
			'icon_documents', 
			'icon_download', 
			'icon_drawer_alt', 
			'icon_drawer', 
			'icon_drive_alt', 
			'icon_drive', 
			'icon_easel_alt', 
			'icon_easel', 
			'icon_error-circle_alt', 
			'icon_error-circle', 
			'icon_error-oct_alt', 
			'icon_error-oct', 
			'icon_error-triangle_alt', 
			'icon_error-triangle', 
			'icon_film', 
			'icon_floppy_alt', 
			'icon_floppy', 
			'icon_flowchart_alt', 
			'icon_flowchart', 
			'icon_folder_download', 
			'icon_folder_upload', 
			'icon_folder-add_alt', 
			'icon_folder-add', 
			'icon_folder-alt', 
			'icon_folder-open_alt', 
			'icon_folder-open', 
			'icon_folder', 
			'icon_genius', 
			'icon_gift_alt', 
			'icon_gift', 
			'icon_globe_alt', 
			'icon_globe-2', 
			'icon_globe', 
			'icon_group', 
			'icon_headphones', 
			'icon_heart_alt', 
			'icon_heart', 
			'icon_hourglass', 
			'icon_house_alt', 
			'icon_house', 
			'icon_id_alt', 
			'icon_id-2_alt', 
			'icon_id-2', 
			'icon_id', 
			'icon_image', 
			'icon_images', 
			'icon_info_alt', 
			'icon_info', 
			'icon_key_alt', 
			'icon_key', 
			'icon_laptop', 
			'icon_lifesaver', 
			'icon_lightbulb_alt', 
			'icon_lightbulb', 
			'icon_like_alt', 
			'icon_like', 
			'icon_link_alt', 
			'icon_link', 
			'icon_loading', 
			'icon_lock_alt', 
			'icon_lock-open_alt', 
			'icon_lock-open', 
			'icon_lock', 
			'icon_mail_alt', 
			'icon_mail', 
			'icon_map_alt', 
			'icon_map', 
			'icon_menu-circle_alt', 
			'icon_menu-circle_alt2', 
			'icon_menu-square_alt', 
			'icon_menu-square_alt2', 
			'icon_menu', 
			'icon_mic_alt', 
			'icon_mic', 
			'icon_minus_alt', 
			'icon_minus_alt2', 
			'icon_minus-06', 
			'icon_minus-box', 
			'icon_mobile', 
			'icon_mug_alt', 
			'icon_mug', 
			'icon_music', 
			'icon_ol', 
			'icon_paperclip', 
			'icon_pause_alt', 
			'icon_pause_alt2', 
			'icon_pause', 
			'icon_pencil_alt', 
			'icon_pencil-edit_alt', 
			'icon_pencil-edit', 
			'icon_pencil', 
			'icon_pens_alt', 
			'icon_pens', 
			'icon_percent_alt', 
			'icon_percent', 
			'icon_phone', 
			'icon_piechart', 
			'icon_pin_alt', 
			'icon_pin', 
			'icon_plus_alt', 
			'icon_plus_alt2', 
			'icon_plus-box', 
			'icon_plus', 
			'icon_printer-alt', 
			'icon_printer', 
			'icon_profile', 
			'icon_pushpin_alt', 
			'icon_pushpin', 
			'icon_puzzle_alt', 
			'icon_puzzle', 
			'icon_question_alt', 
			'icon_question_alt2', 
			'icon_question', 
			'icon_quotations_alt', 
			'icon_quotations_alt2', 
			'icon_quotations', 
			'icon_refresh', 
			'icon_ribbon_alt', 
			'icon_ribbon', 
			'icon_rook', 
			'icon_search_alt', 
			'icon_search-2', 
			'icon_search', 
			'icon_shield_alt', 
			'icon_shield', 
			'icon_star_alt', 
			'icon_star-half_alt', 
			'icon_star-half', 
			'icon_star', 
			'icon_stop_alt', 
			'icon_stop_alt2', 
			'icon_stop', 
			'icon_table', 
			'icon_tablet', 
			'icon_tag_alt', 
			'icon_tag', 
			'icon_tags_alt', 
			'icon_tags', 
			'icon_target', 
			'icon_tool', 
			'icon_toolbox_alt', 
			'icon_toolbox', 
			'icon_tools', 
			'icon_trash_alt', 
			'icon_trash', 
			'icon_ul', 
			'icon_upload', 
			'icon_vol-mute_alt', 
			'icon_vol-mute', 
			'icon_volume-high_alt', 
			'icon_volume-high', 
			'icon_volume-low_alt', 
			'icon_volume-low', 
			'icon_wallet_alt', 
			'icon_wallet', 
			'icon_zoom-in_alt', 
			'icon_zoom-in', 
			'icon_zoom-out_alt', 
			'icon_zoom-out', 
			'social_blogger_circle', 
			'social_blogger_square', 
			'social_blogger', 
			'social_delicious_circle', 
			'social_delicious_square', 
			'social_delicious', 
			'social_deviantart_circle', 
			'social_deviantart_square', 
			'social_deviantart', 
			'social_dribbble_circle', 
			'social_dribbble_square', 
			'social_dribbble', 
			'social_facebook_circle', 
			'social_facebook_square', 
			'social_facebook', 
			'social_flickr_circle', 
			'social_flickr_square', 
			'social_flickr', 
			'social_googledrive_alt2', 
			'social_googledrive_square', 
			'social_googledrive', 
			'social_googleplus_circle', 
			'social_googleplus_square', 
			'social_googleplus', 
			'social_instagram_circle', 
			'social_instagram_square',  
			'social_instagram', 
			'social_linkedin_circle', 
			'social_linkedin_square', 
			'social_linkedin', 
			'social_myspace_circle', 
			'social_myspace_square', 
			'social_myspace', 
			'social_picassa_circle', 
			'social_picassa_square', 
			'social_picassa', 
			'social_pinterest_circle', 
			'social_pinterest_square', 
			'social_pinterest', 
			'social_rss_circle', 
			'social_rss_square', 
			'social_rss', 
			'social_share_circle', 
			'social_share_square', 
			'social_share', 
			'social_skype_circle',  
			'social_skype_square', 
			'social_skype', 
			'social_spotify_circle',  
			'social_spotify_square', 
			'social_spotify', 
			'social_stumbleupon_circle', 
			'social_stumbleupon_square', 
			'social_tumbleupon', 
			'social_tumblr_circle', 
			'social_tumblr_square',  
			'social_tumblr', 
			'social_twitter_circle', 
			'social_twitter_square',  
			'social_twitter', 
			'social_vimeo_circle', 
			'social_vimeo_square', 
			'social_vimeo', 
			'social_wordpress_circle',  
			'social_wordpress_square', 
			'social_wordpress', 
			'social_youtube_circle', 
			'social_youtube_square',  
			'social_youtube'
		);
		return $icons;	
	}
}

/**
 * Collect and display page title markup and content
 */
if(!( function_exists('ebor_the_page_title') )){
	function ebor_the_page_title(){
?>
		
		<section class="title-text-divider ebor-page-header">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
						<?php 
							the_title('<h2>', '</h2>'); 
							ebor_the_subtitle('<h4>', '</h4>');
						?>
					</div>
				</div><!--end of row-->
			</div><!--end of container-->
		</section>
		
<?php
	}	
}

if(!( function_exists('ebor_the_page_title_alt') )){
	function ebor_the_page_title_alt($image = false){
?>
		
		<section class="hero-slider large-image short-header fixed-header">
			<ul class="slides">
				<li>
				
					<div class="background-image-holder background-parallax">
						<img alt="Slide Background" class="background-image" src="<?php echo esc_url($image); ?>">
					</div>
		
					<div class="container vertical-align">
						<div class="row">
							<div class="col-sm-12 text-center">
								<?php
									the_title('<h1 class="text-white">', '</h1>'); 
									ebor_the_subtitle('<h4 class="text-white">', '</h4>');
								?>
							</div>
						</div>
					</div><!--end of container-->	
					
				</li>
			</ul>
		</section>
		
<?php
	}	
}

/**
 * Custom Comment Markup for Machine
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_custom_comment') )){
	function ebor_custom_comment($comment, $args, $depth) { 
		$GLOBALS['comment'] = $comment; 
	?>
		
		<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
			<div class="blog-comment">
			
				<div class="user">
					<?php echo get_avatar( $comment->comment_author_email, 100 ); ?>
				</div>
	
				<div class="info">
					<?php printf('<span class="title">%s</span>', get_comment_author_link()); ?>
					<span class="date"><?php echo get_comment_date(); ?></span>
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
					<?php echo wpautop( htmlspecialchars_decode( get_comment_text() ) ); ?>
					<?php if ($comment->comment_approved == '0') : ?>
					<p><em><?php _e('Your comment is awaiting moderation.', 'machine') ?></em></p>
					<?php endif; ?>
				</div>
				
			</div>
	
	<?php }
}