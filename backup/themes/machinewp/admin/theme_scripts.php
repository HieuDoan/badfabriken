<?php 

/**
 * Ebor Load Scripts
 * Properly Enqueues Scripts & Styles for the theme
 * @since version 1.0
 * @author TommusRhodus
 */ 
if(!( function_exists('ebor_load_scripts') )){
	function ebor_load_scripts() {
		
		$protocol = is_ssl() ? 'https' : 'http';
		$body_font = get_option('body_font_url', $protocol . '://fonts.googleapis.com/css?family=Roboto:100,400,300,700,400italic,500');
		$alt_font = get_option('alt_font_url', $protocol . '://fonts.googleapis.com/css?family=Montserrat:400,700');
		
		//Enqueue Fonts
		if( $body_font )
			wp_enqueue_style( 'ebor-body-font', $body_font );
			
		if( $alt_font )
			wp_enqueue_style( 'ebor-alt-font', $alt_font );
			
		//Enqueue Styles
		wp_enqueue_style( 'ebor-bootstrap', EBOR_THEME_DIRECTORY . 'style/css/bootstrap.min.css' );
		wp_enqueue_style( 'ebor-fonts', EBOR_THEME_DIRECTORY . 'style/css/fonts.css' );	
		wp_enqueue_style( 'ebor-plugins', EBOR_THEME_DIRECTORY . 'style/css/plugins.css' );	
		wp_enqueue_style( 'ebor-theme-styles', EBOR_THEME_DIRECTORY . 'style/css/theme.less' );
		wp_enqueue_style( 'ebor-style', get_stylesheet_uri() );
		
		//Enqueue Scripts
		wp_enqueue_script( 'ebor-bootstrap', EBOR_THEME_DIRECTORY . 'style/js/bootstrap.min.js', array('jquery'), false, true  );
		wp_enqueue_script( 'ebor-plugins', EBOR_THEME_DIRECTORY . 'style/js/plugins.js', array('jquery'), false, true  );
		wp_enqueue_script( 'ebor-scripts', EBOR_THEME_DIRECTORY . 'style/js/scripts.js', array('jquery'), false, true  );
		
		//Enqueue Comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
		if( 'full-width' == get_option('site_width', 'boxed') )
			wp_add_inline_style('ebor-style','.nav-container,.nav-container nav,.main-container{width:100%;max-width:100%;}');
		
		//Add custom CSS ability
		wp_add_inline_style( 'ebor-style', get_option('custom_css') );
		
		/**
		 * localize script
		 */
		$script_data = array( 
			'access_token'       => get_option('instagram_token', ''),
			'client_id'          => get_option('instagram_client', ''),
		);
		wp_localize_script( 'ebor-scripts', 'wp_data', $script_data );
	}
	add_action('wp_enqueue_scripts', 'ebor_load_scripts', 110);
}

/**
 * Ebor Load Admin Scripts
 * Properly Enqueues Scripts & Styles for the theme
 * 
 * @since version 1.0
 * @author TommusRhodus
 */
if(!( function_exists('ebor_admin_load_scripts') )){
	function ebor_admin_load_scripts(){
		wp_enqueue_style('ebor-theme-admin-css', EBOR_THEME_DIRECTORY . 'admin/ebor-theme-admin.css');
		wp_enqueue_style('ebor-fonts', EBOR_THEME_DIRECTORY . 'style/css/fonts.css');	
		wp_enqueue_script('ebor-theme-admin-js', EBOR_THEME_DIRECTORY . 'admin/ebor-theme-admin.js', array('jquery'), false, true);
	}
	add_action('admin_enqueue_scripts', 'ebor_admin_load_scripts', 200);
}