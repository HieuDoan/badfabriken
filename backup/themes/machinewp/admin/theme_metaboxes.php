<?php 

/**
 * Build theme metaboxes
 * Uses the cmb metaboxes class found in the ebor framework plugin
 * More details here: https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if(!( function_exists('ebor_custom_metaboxes') )){
	function ebor_custom_metaboxes( $meta_boxes ) {
		
		/**
		 * Setup variables
		 */
		$prefix = '_ebor_';
		$footer_options = ebor_get_footer_options();
		$header_options = ebor_get_header_options();
		$social_options = ebor_get_social_icons();
		
		$footer_overrides['none'] = 'Do Not Override Footer Option On This Page';
		foreach( $footer_options as $key => $value ){
			$footer_overrides[$key] = 'Override Footer: ' . $value; 	
		}
		
		$header_overrides['none'] = 'Do Not Override Header Option On This Page';
		foreach( $header_options as $key => $value ){
			$header_overrides[$key] = 'Override Header: ' . $value; 	
		}
		
		/**
		 * Subtitles
		 */
		$meta_boxes[] = array(
			'id' => 'subtitle_metabox',
			'title' => __('The Subtitle', 'machine'),
			'object_types' => array('team', 'post', 'portfolio', 'page'), // post type
			'context' => 'normal',
			'priority' => 'low',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name' => __('Subtitle', 'machine'),
					'desc' => '(Optional) Enter a subtitle for this post / page.',
					'id'   => $prefix . 'the_subtitle',
					'type' => 'text',
				)
			)
		);
		
		/**
		 * Social Icons for Team Members
		 */
		$meta_boxes[] = array(
			'id' => 'social_metabox',
			'title' => __('Team Member Details', 'machine'),
			'object_types' => array('team'), // post type
			'context' => 'normal',
			'priority' => 'high',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name' => __('Job Title', 'machine'),
					'desc' => '(Optional) Enter a Job Title for this Team Member',
					'id'   => $prefix . 'the_job_title',
					'type' => 'text',
				),
				array(
				    'id'          => $prefix . 'team_social_icons',
				    'type'        => 'group',
				    'options'     => array(
				        'add_button'    => __( 'Add Another Icon', 'meetup' ),
				        'remove_button' => __( 'Remove Icon', 'meetup' ),
				        'sortable'      => true
				    ),
				    'fields' => array(
						array(
							'name' => 'Social Icon',
							'desc' => 'What icon would you like for this team members first social profile?',
							'id' => $prefix . 'social_icon',
							'type' => 'select',
							'options' => $social_options
						),
						array(
							'name' => __('URL for Social Icon', 'meetup'),
							'desc' => __("Enter the URL for Social Icon 1 e.g www.google.com", 'meetup'),
							'id'   => $prefix . 'social_icon_url',
							'type' => 'text_url',
						),
				    ),
				),
			)
		);
		
		/**
		 * Sidebar on/off
		 */
		$meta_boxes[] = array(
			'id' => 'post_metabox',
			'title' => __('The Post Additional Details', 'machine'),
			'object_types' => array('post'), // post type
			'context' => 'normal',
			'priority' => 'low',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name' => __('Disable Post Sidebar','machine'),
					'desc' => __("Check to disable the sidebar on this post.", 'machine'),
					'id'   => $prefix . 'disable_sidebar',
					'type' => 'checkbox',
				),
				array(
					'name' => __('Use featured image as header image?','machine'),
					'id'   => $prefix . 'enable_header',
					'type' => 'checkbox',
				),
			)
		);
		
		/**
		 * Post & Portfolio Header Images
		 */
		$meta_boxes[] = array(
			'id' => 'post_header_metabox',
			'title' => __('Page Overrides', 'pivot'),
			'object_types' => array('page', 'team', 'post', 'portfolio'), // post type
			'context' => 'normal',
			'priority' => 'low',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name'         => __( 'Override Header?', 'pivot' ),
					'desc'         => __( 'Header Layout is set in "appearance" -> "customise". To override this for this page only, use this control.', 'pivot' ),
					'id'           => $prefix . 'header_override',
					'type'         => 'select',
					'options'      => $header_overrides,
					'std'          => 'none'
				),
				array(
					'name'         => __( 'Override Footer?', 'pivot' ),
					'desc'         => __( 'Footer Layout is set in "appearance" -> "customise". To override this for this page only, use this control.', 'pivot' ),
					'id'           => $prefix . 'footer_override',
					'type'         => 'select',
					'options'      => $footer_overrides,
					'std'          => 'none'
				),
			)
		);
		
		return $meta_boxes;
	}
	add_filter( 'cmb2_meta_boxes', 'ebor_custom_metaboxes' );
}