<?php 

if(!( function_exists('ebor_cart_icon') )){
	function ebor_cart_icon() {
		global $woocommerce;	
		echo '<li class="ebor-cart"><a href="'. wc_get_cart_url() .'"><i class="icon icon_cart"></i><span class="ebor-count woocommerce">0</span></a></li>';
	}
}

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
if(!( function_exists('ebor_woocommerce_header_add_to_cart_fragment_total') )){
	function ebor_woocommerce_header_add_to_cart_fragment_total( $fragments ) {
		global $woocommerce;
		ob_start();
		echo '<span class="ebor-count woocommerce">'. $woocommerce->cart->get_cart_contents_count() .'</span>';	
		$fragments['span.ebor-count.woocommerce'] = ob_get_clean();
		return $fragments;
	}
	add_filter('add_to_cart_fragments', 'ebor_woocommerce_header_add_to_cart_fragment_total');
}

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'ebor_dequeue_styles' );
function ebor_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );
	return $enqueue_styles;
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_add_to_cart', 11 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_meta', 12 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );

if(!( function_exists('ebor_rating_html') )){
	function ebor_rating_html($html = false, $count = false){
		global $product;
		$average      = $product->get_average_rating();
		
		if($count)
			$average = $count;
		
		$stars        = ( round($average * 2) / 2 );
		
		$before = '<div class="reviews" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<ul class="stars">';
			
		$stars_html = false;
		
		if( 0.5 == $stars ){
			$stars_html = '<li><i class="icon_star-half"></i></li>';	
		} elseif( 1 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li>';	
		} elseif( 1.5 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star-half"></i></li>';	
		} elseif( 2 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li>';	
		} elseif( 2.5 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star-half"></i></li>';	
		} elseif( 3 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li>';	
		} elseif( 3.5 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star-half"></i></li>';	
		} elseif( 4 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li>';	
		} elseif( 4.5 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star-half"></i></li>';	
		} elseif( 5 == $stars ){
			$stars_html = '<li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li><li><i class="icon_star"></i></li>';	
		}
		
		$after = '</ul></div>';
		return $before . $stars_html . $after;
	}
	add_filter( 'woocommerce_product_get_rating_html', 'ebor_rating_html', 99 );
}