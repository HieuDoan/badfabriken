<?php 

/**
 * Build theme options
 * Uses the Ebor_Options class found in the ebor-framework plugin
 * Panels are WP 4.0+!!!
 * 
 * @since 1.0.0
 * @author tommusrhodus
 */
if( class_exists('Ebor_Options') ){
	$ebor_options = new Ebor_Options;
	
	/**
	 * Variables
	 */
	$theme = wp_get_theme();
	$theme_name = $theme->get( 'Name' );
	$footer_default = '<a href="http://www.tommusrhodus.com">Machine Premium WordPress Theme by TommusRhodus</a>';
	$portfolio_layouts = ebor_get_portfolio_layouts();
	$footer_layouts = ebor_get_footer_options();
	$header_layouts = ebor_get_header_options();
	$blog_layouts = array(
		'grid' => 'Grid Blog',
		'grid-sidebar' => 'Grid Blog Sidebar',
		'classic' => 'Classic Blog',
		'classic-sidebar' => 'Classic Blog Sidebar'
	);
	
	$social_options = ebor_get_social_icons();
	$fonts_description = 'Fonts: ' . $theme_name . ' uses Google Fonts, <a href="https://www.google.com/fonts" target="_blank">all of which are viewable here</a>. Unlike some themes, ' . $theme_name . ' does not load all of these fonts into these options, in avoiding this ' . $theme_name . ' can work faster and more reliably.<br /><br />
	
	To customize the fonts on your website use the URL above and the inputs below accordingly. Full details of this process (and the default values) can be found in the theme documentation!';
	
	/**
	 * Default stuff
	 * 
	 * Each of these is a default option that appears in each theme, demo data, favicons and a custom css input
	 * 
	 * @since 1.0.0
	 * @author tommusrhodus
	 */
	$ebor_options->add_panel( $theme_name . ': Demo Data', 5, '');
	$ebor_options->add_panel( $theme_name . ': Styling Settings', 205, 'All of the controls in this section directly relate to the styling page of ' . $theme_name);
	$ebor_options->add_section('demo_data_section', 'Import Demo Data', 10, $theme_name . ': Demo Data', '<strong>Please read this before importing demo data via this control:</strong><br /><br />The demo data this will install includes images from my demo site with <strong>heavy blurring applied</strong> this is due to licensing restrictions. Simply replace these images with your own.<br /><br />Note that this process can take up to 15mins on slower servers, go make a cup of tea. If you havn\'t had a notification in 30mins, use the fallback method outlined in the written documentation.<br /><br />');
	$ebor_options->add_section('favicon_section', 'Favicons', 30, $theme_name . ': Styling Settings');
	$ebor_options->add_section('custom_css_section', 'Custom CSS', 40, $theme_name . ': Styling Settings');
	$ebor_options->add_setting('image', 'custom_favicon', 'Custom Favicon Upload (Use .png)', 'favicon_section', '', 10);
	$ebor_options->add_setting('image', 'mobile_favicon', 'Mobile Favicon Upload (Use .png)', 'favicon_section', '', 15);
	$ebor_options->add_setting('image', '72_favicon', '72x72px Favicon Upload (Use .png)', 'favicon_section', '', 20);
	$ebor_options->add_setting('image', '114_favicon', '114x114px Favicon Upload (Use .png)', 'favicon_section', '', 25);
	$ebor_options->add_setting('image', '144_favicon', '144x144px Favicon Upload (Use .png)', 'favicon_section', '', 30);
	$ebor_options->add_setting('demo_import', 'demo_import', 'Import Demo Data', 'demo_data_section', '', 10);
	$ebor_options->add_setting('textarea', 'custom_css', 'Custom CSS', 'custom_css_section', '', 30);
	
	/**
	 * Panels
	 * 
	 * add_panel($name, $priority, $description)
	 * 
	 * @since 1.0.0
	 * @author tommusrhodus
	 */
	$ebor_options->add_panel( $theme_name . ': Header Settings', 215, 'All of the controls in this section directly relate to the header and logos of ' . $theme_name);
	$ebor_options->add_panel( $theme_name . ': Blog Settings', 225, 'All of the controls in this section directly relate to the control of blog items within ' . $theme_name);
	$ebor_options->add_panel( $theme_name . ': Portfolio Settings', 230, 'All of the controls in this section directly relate to the control of portfolio items within ' . $theme_name);
	$ebor_options->add_panel( $theme_name . ': Footer Settings', 290, 'All of the controls in this section directly relate to the control of the footer within ' . $theme_name);
	
	/**
	 * Sections
	 * 
	 * add_section($name, $title, $priority, $panel, $description)
	 * 
	 * @since 1.0.0
	 * @author tommusrhodus
	 */
	//Styling
	$ebor_options->add_section('fonts_section', 'Fonts', 5, $theme_name . ': Styling Settings', $fonts_description);
	$ebor_options->add_section('site_section', 'Site Width', 1, $theme_name . ': Styling Settings');
	
	//Blog Sections
	$ebor_options->add_section('blog_settings', 'Blog Settings', 1, $theme_name . ': Blog Settings');
	$ebor_options->add_section('blog_text_section', 'Blog Texts', 5, $theme_name . ': Blog Settings');
	$ebor_options->add_section('blog_layout_section', 'Blog Layout', 10, $theme_name . ': Blog Settings');
	
	//Portfolio Sections
	$ebor_options->add_section('portfolio_text_section', 'Portfolio Texts', 5, $theme_name . ': Portfolio Settings');
	$ebor_options->add_section('portfolio_layout_section', 'Portfolio Layout', 10, $theme_name . ': Portfolio Settings');
	
	//Header Settings
	$ebor_options->add_section('logo_settings_section', 'Logo Settings', 10, $theme_name . ': Header Settings');
	$ebor_options->add_section('header_social_settings_section', 'Icon Settings', 15, $theme_name . ': Header Settings');
	$ebor_options->add_section('footer_social_settings_section', 'Footer Icons Settings', 40, $theme_name . ': Footer Settings', '');
	$ebor_options->add_section('header_layout_section', 'Header Layout', 5, $theme_name . ': Header Settings', 'This setting controls the theme header site-wide. If you need to you can override this setting on specific posts and pages from within that posts edit screen.<br /><br /><strong>PLEASE NOTE:</strong> the default "animated" dropdown style is not compatible with the mega menu, please set to "standard" to use any mega menu functions.');
	
	//Footer Settings
	$ebor_options->add_section('footer_layout_section', 'Footer Layout', 5, $theme_name . ': Footer Settings', 'This setting controls the theme footer site-wide. If you need to you can override this setting on specific posts and pages from within that posts edit screen.');
	$ebor_options->add_section('subfooter_settings_section', 'Sub-Footer Settings', 30, $theme_name . ': Footer Settings');
	$ebor_options->add_section('footer_social_settings_section', 'Footer Icons Settings', 40, $theme_name . ': Footer Settings', 'These social icons are only shown in certain footer layouts.');
	
	/**
	 * Settings (The Actual Options)
	 * Repeated settings are stepped using a for() loop and counter
	 * 
	 * add_setting($type, $option, $title, $section, $default, $priority, $select_options)
	 * 
	 * @since 1.0.0
	 * @author tommusrhodus
	 */
	 
	$ebor_options->add_setting('select', 'site_width', 'Site Width', 'site_section', 'boxed', 10, array('boxed' => 'Boxed', 'full-width' => 'Full Width'));
	
	//Fonts
	$ebor_options->add_setting('input', 'body_font', 'Body Font', 'fonts_section', 'Roboto', 10);
	$ebor_options->add_setting('textarea', 'body_font_url', 'Body Font URL Parameter', 'fonts_section', 'http://fonts.googleapis.com/css?family=Roboto:100,400,300,700,400italic,500', 15);
	$ebor_options->add_setting('input', 'alt_font', 'Alternate Font', 'fonts_section', 'Montserrat', 40);
	$ebor_options->add_setting('textarea', 'alt_font_url', 'Alternate Font URL Parameter', 'fonts_section', 'http://fonts.googleapis.com/css?family=Montserrat:400,700', 45);
	
	//Colour Options
	$ebor_options->add_setting('color', 'color-dark-bg', 'Background Colour Dark', 'colors', '#222222', 5);
	$ebor_options->add_setting('color', 'color-primary', 'Highlight Colour', 'colors', '#0054a6', 10);
	$ebor_options->add_setting('color', 'color-body', 'Body Font Colour', 'colors', '#777777', 15);
	$ebor_options->add_setting('color', 'color-heading', 'Headings Font Colour', 'colors', '#333333', 20);

	//Blog Options
	$ebor_options->add_setting('select', 'blog_layout', 'Blog Index Layout', 'blog_layout_section', 'classic', 10, $blog_layouts);
	$ebor_options->add_setting('input', 'blog_read_more', 'Blog Read More Text', 'blog_text_section', 'Read More', 20);
	$ebor_options->add_setting('input', 'blog_title', 'Blog Title', 'blog_text_section', 'Our Blog', 25);
	$ebor_options->add_setting('image', 'blog_header', 'Blog Title Background Image', 'blog_text_section', '', 27);
	$ebor_options->add_setting('input', 'comments_title', 'Comments Title', 'blog_text_section', 'Would you like to share your thoughts?', 30);
	$ebor_options->add_setting('input', 'comments_subtitle', 'Comments Subtitle', 'blog_text_section', 'Your email address will not be published. Required fields are marked *', 35);

	//Portfolio options
	$ebor_options->add_setting('select', 'portfolio_layout', 'Portfolio Layout', 'portfolio_layout_section', 'grid', 10, $portfolio_layouts);
	$ebor_options->add_setting('input', 'portfolio_title', 'Portfolio Archives: Title', 'portfolio_text_section', 'Our Portfolio', 20);
	
	//Logo Options
	$ebor_options->add_setting('image', 'custom_logo', 'Logo', 'logo_settings_section', EBOR_THEME_DIRECTORY . 'style/img/logo-square-light.png', 5);
	$ebor_options->add_setting('image', 'custom_logo_dark', 'Dark Logo', 'logo_settings_section', EBOR_THEME_DIRECTORY . 'style/img/logo-square-dark.png', 10);
	
	//Footer Options
	$ebor_options->add_setting('textarea', 'copyright', 'Copyright Message', 'subfooter_settings_section', $footer_default, 20);
	
	//Header Layout Option
	$ebor_options->add_setting('select', 'header_layout', 'Global Header Layout', 'header_layout_section', 'bar', 5, $header_layouts);
	$ebor_options->add_setting('select', 'dropdown_style', 'Dropdown Menu Style', 'header_layout_section', 'standard-drops', 10, array('rare-drops' => 'Animated', 'standard-drops' => 'Standard'));
	$ebor_options->add_setting('select', 'footer_layout', 'Global Footer Layout', 'footer_layout_section', 'classic', 5, $footer_layouts);
	
	for( $i = 1; $i < 4; $i++ ){
		$ebor_options->add_setting('select', 'header_social_icon_' . $i, 'Header Social Icon ' . $i, 'header_social_settings_section', 'none', 20 + $i + $i, $social_options);
		$ebor_options->add_setting('input', 'header_social_url_' . $i, 'Header Social URL ' . $i, 'header_social_settings_section', '', 21 + $i + $i);
	}
	
	//Footer Icons
	for( $i = 1; $i < 7; $i++ ){
		$ebor_options->add_setting('select', 'footer_social_icon_' . $i, 'Footer Social Icon ' . $i, 'footer_social_settings_section', 'none', 20 + $i + $i, $social_options);
		$ebor_options->add_setting('input', 'footer_social_url_' . $i, 'Footer Social URL ' . $i, 'footer_social_settings_section', '', 21 + $i + $i);
	}
	
	/**
	 * Instagram API Stuff
	 */
	$ebor_options->add_section('instagram_api_section', $theme_name . ': Instagram Settings', 340, false, '<code>IMPORTANT NOTE:</code> This is the Instagram setup section for the theme, it requires an Access Token and Client ID.<br /><br />Due to how Instagram have set their API you have to register as a developer with Instagram for this to work.<br /><br />For setup details, <a href="https://tommusrhodus.ticksy.com/article/7566" target="_blank">please read this</a>');
	$ebor_options->add_setting('input', 'instagram_token', 'Instagram Access Token', 'instagram_api_section', '', 5);
	$ebor_options->add_setting('input', 'instagram_client', 'Instagram Client ID', 'instagram_api_section', '', 10);
}