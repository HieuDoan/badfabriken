<?php 

/**
 * Here is all the custom colours for the theme.
 * $handle is a reference to the handle used with wp_enqueue_style()
 */
if(!( function_exists('ebor_less_vars') )){ 
	function ebor_less_vars( $vars, $handle = 'ebor-theme-styles' ) {
		$vars['alt-font']            = get_option('alt_font', 'Montserrat');
		$vars['body-font']           = get_option('body_font', 'Roboto');
		$vars['color-body']          = get_option('color-body', '#777777');
		$vars['color-heading']       = get_option('color-heading', '#333333');
	    $vars['color-primary']       = get_option('color-primary', '#0054a6');
	    $vars['color-love']          = get_option('color-love', '#e21a1a');
	    $vars['dark-bg']             = get_option('color-dark-bg', '#222222');
	    $vars['standard-space']      = get_option('standard-space', '80') . 'px';
	    return $vars;
	}
	add_filter( 'less_vars', 'ebor_less_vars', 10, 2 );
}

/**
 * Force easy google fonts plugin styles
 */
if(!( function_exists('ebor_egf_force_styles') )){ 
	function ebor_egf_force_styles( $force_styles ) {
	    return true;
	}
	add_filter( 'tt_font_force_styles', 'ebor_egf_force_styles' );
}

/**
 * Add a clearfix to the end of the_content()
 */
if(!( function_exists('ebor_add_clearfix') )){ 
	function ebor_add_clearfix( $content ) { 
		if( is_single() )
	   		$content = $content .= '<div class="clearfix"></div>';
	    return $content;
	}
	add_filter( 'the_content', 'ebor_add_clearfix' ); 
}

/**
 * Control default more
 */
if(!( function_exists('ebor_excerpt_more') )){
	function ebor_excerpt_more( $more ) {
		return '...';
	}
	add_filter('excerpt_more', 'ebor_excerpt_more');
}

/**
 * Control default excerpt length.
 */
if(!( function_exists('ebor_excerpt_length') )){
	function ebor_excerpt_length( $length ) {
		return 21;
	}
	add_filter( 'excerpt_length', 'ebor_excerpt_length', 999 );
}

/**
 * Remove leading whitespace from the_excerpt
 */
if(!( function_exists('ebor_ltrim_excerpt') )){
	function ebor_ltrim_excerpt( $excerpt ) {
	    return preg_replace( '~^(\s*(?:&nbsp;)?)*~i', '', $excerpt );
	}
	add_filter( 'get_the_excerpt', 'ebor_ltrim_excerpt' );
}

/**
 * Add additional settings to gallery shortcode
 */
if(!( function_exists('ebor_add_gallery_settings') )){ 
	function ebor_add_gallery_settings(){
	?>
	
		<script type="text/html" id="tmpl-machine-gallery-setting">
			<h3>machine Theme Gallery Settings</h3>
			<label class="setting">
				<span><?php _e('Gallery Layout', 'machine'); ?></span>
				<select data-setting="layout">
					<option value="default">Default Layout</option>
					<option value="slider">Machine Slider</option>        
					<option value="lightbox">Machine Lightbox Gallery</option> 
				</select>
			</label>
		</script>
	
		<script>
			jQuery(document).ready(function(){
				_.extend(wp.media.gallery.defaults, { layout: 'default' });
				
				wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
					template: function(view){
					  return wp.media.template('gallery-settings')(view)
					       + wp.media.template('machine-gallery-setting')(view);
					}
				});
			});
		</script>
	  
	<?php
	}
	add_action('print_media_templates', 'ebor_add_gallery_settings');
}


/**
 * Custom gallery shortcode
 *
 * Filters the standard WordPress gallery shortcode.
 *
 * @since 1.0.0
 */
if(!( function_exists('ebor_post_gallery') )){
	function ebor_post_gallery( $output, $attr) {
		
		global $post, $wp_locale;
	
	    static $instance = 0;
	    $instance++;
	
	    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	    if ( isset( $attr['orderby'] ) ) {
	        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
	        if ( !$attr['orderby'] )
	            unset( $attr['orderby'] );
	    }
	
	    extract(shortcode_atts(array(
	        'order'      => 'ASC',
	        'orderby'    => 'menu_order ID',
	        'id'         => $post->ID,
	        'itemtag'    => 'div',
	        'icontag'    => 'dt',
	        'captiontag' => 'dd',
	        'columns'    => 3,
	        'size'       => 'large',
	        'include'    => '',
	        'exclude'    => '',
	        'layout'     => ''
	    ), $attr));
	
	    $id = intval($id);
	    if ( 'RAND' == $order )
	        $orderby = 'none';
	
	    if ( !empty($include) ) {
	        $include = preg_replace( '/[^0-9,]+/', '', $include );
	        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	
	        $attachments = array();
	        foreach ( $_attachments as $key => $val ) {
	            $attachments[$val->ID] = $_attachments[$key];
	        }
	    } elseif ( !empty($exclude) ) {
	        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
	        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	    } else {
	        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	    }
	
	    if ( empty($attachments) )
	        return '';
	
	    if ( is_feed() ) {
	        $output = "\n";
	        foreach ( $attachments as $att_id => $attachment )
	            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
	        return $output;
	    }
	    
	    /**
	     * Return Lightbox Layout
	     */
	    if( $layout == 'lightbox' ){
		   $output = '<div class="contained-projects"><div class="container-fluid"><div class="row">';
		   
		   foreach ( $attachments as $id => $attachment ) {
		       $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_url($id, 'full', false, false) : wp_get_attachment_url($id, 'full', true, false);
		   	   $item = get_post($id);
		   		
		   			$output .= '<div class="col-md-4 col-sm-6 project">
		   			
		   				<a href="'. $link .'" data-lightbox="true" data-title="'. $item->post_title .'">
		   					<div class="background-image-holder fadeIn">
		   						' . wp_get_attachment_image($id, 'medium') . '
		   					</div>
		   				</a>
		   				
		   				<h2>'. $item->post_title .'</h2>
		   				'. wpautop(htmlspecialchars_decode($item->post_excerpt)) .'
		   				
		   			</div>';
		   }
		   
		   $output .= '</div></div></div>';
		   return $output;
	    }
	    
	    /**
	     * Return Slider Layout
	     */
	    if( $layout == 'slider' ){
	    	$output = '<div class="post-slider"><ul class="slides">';
	    		foreach ( $attachments as $id => $attachment ) {
	    			$image = wp_get_attachment_image_src($id, 'large');
	    			if(!( isset($image[0]) ))
	    				$image[0] = false;
	    		    $output .= '<li><img src="'. esc_url($image[0]) .'" alt="'. get_the_title() .'" /></li>';
	    		} 
	    	$output .= '</ul></div>';
	    	return $output;
	    }
	    
	}
	add_filter( 'post_gallery', 'ebor_post_gallery', 10, 2 );
}