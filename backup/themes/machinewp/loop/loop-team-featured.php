<?php 
	global $wp_query;
	
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		
		$class = ( ($wp_query->current_post + 1) % 2 == 0 ) ? 'dark-bg' : '';
		echo '<section class="team-1-single '. esc_attr($class) .'">';
		
		/**
		 * Get blog posts by blog layout.
		 */
		get_template_part('loop/content', 'team-featured');
		
		echo '</section>';
	
	endwhile;	
	else : 
		
		/**
		 * Display no posts message if none are found.
		 */
		get_template_part('loop/content','none');
		
	endif;