<div id="post-<?php esc_attr(the_ID()); ?>" <?php post_class('blog-snippet-2'); ?>>

	<?php if( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>">
			<div class="background-image-holder">
				<?php the_post_thumbnail('large', array('class' => 'background-image')); ?>
			</div>
		</a>
	<?php endif; ?>
	
	<div class="description">
		<div class="meta">
			<span class="date">
				<?php 
					the_time(get_option('date_format'));
					_e(' by ','machine'); 
					the_author_posts_link();
				?>
				</span>
			<?php the_tags('<ul class="tag-list"><li>','</li><li>','</li></ul>'); ?>
		</div>
		<?php 
			the_title('<h3>', '</h3>');
			the_excerpt();
		?>
		<div class="clearfix"></div>
		<a href="<?php the_permalink(); ?>" class="btn btn-sm">
			<?php echo get_option('blog_read_more','Read More'); ?>
		</a>
	</div>
	
</div>