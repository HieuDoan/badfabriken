<?php $cats = get_categories('taxonomy=portfolio_category'); ?>

<section class="contained-projects">
	<div class="container">
	
		<?php 
			if(!is_tax() && !empty($cats) )
				echo ebor_portfolio_filters($cats); 
		?>
	
		<div class="row">
			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					
					/**
					 * Get blog posts by blog layout.
					 */
					get_template_part('loop/content', 'portfolio-grid');
				
				endwhile;	
				else : 
					
					/**
					 * Display no posts message if none are found.
					 */
					get_template_part('loop/content','none');
					
				endif;
			?>	
		</div><!--end of row-->
		
	</div><!--end of container-->
</section>