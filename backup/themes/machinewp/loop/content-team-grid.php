<?php
	global $post;
	$job = get_post_meta( $post->ID, '_ebor_the_job_title', true );
	$icons = get_post_meta( $post->ID, '_ebor_team_social_icons', true );
?>

<div class="col-md-4 col-sm-6 team-member">
	<?php 
		the_post_thumbnail('full');
		the_title('<h3><a href="'. esc_url(get_permalink()) .'">', '</a></h3><span>'. esc_html($job) .'</span>'); 
		the_excerpt();
		
		if( is_array($icons) ){
			echo '<ul class="social-links">';
				foreach( $icons as $key => $icon ){
					if(!( isset( $icon['_ebor_social_icon_url'] ) ))
						continue;
						
					echo '<li><a href="'. esc_url($icon['_ebor_social_icon_url']) .'" target="_blank"><i class="icon '. esc_attr($icon['_ebor_social_icon']) .'"></i></a></li>';
				}
			echo '</ul>';
		}
	?>
</div>