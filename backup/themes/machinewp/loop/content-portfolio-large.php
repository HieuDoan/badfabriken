<div class="project col-sm-6 image-holder">

	<div class="background-image-holder">
		<?php the_post_thumbnail('full', array('class' => 'background-image')); ?>
	</div>
	
	<div class="hover-state text-center">
		<a href="<?php the_permalink(); ?>" class="vertical-align">
			<div class="hover-content">
				<div class="detail-line"></div>
				<?php 
					the_title('<h5 class="text-white">', '</h5>'); 
					ebor_the_subtitle('<h4 class="text-white">', '</h4>');
				?>
				<i class="icon arrow_right"></i>
			</div>
		</a>
	</div>
	
</div><!--end of inidividual project-->