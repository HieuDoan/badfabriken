<?php
	$thumbnail = has_post_thumbnail();
	$class = ( $thumbnail ) ? 'image-snippet overlay' : '';
?>

<div id="post-<?php esc_attr(the_ID()); ?>" <?php post_class('col-md-4 col-sm-6'); ?>>
	<div class="blog-snippet-1 <?php echo esc_attr($class); ?>">
		
		<?php if( $thumbnail ) : ?>
			<div class="background-image-holder">
				<?php the_post_thumbnail('large', array('class' => 'background-image')); ?>
			</div>
		<?php endif; ?>

		<span class="date alt-font uppercase"><?php the_time(get_option('date_format')); ?></span>
		
		<?php 
			the_title('<h5>', '</h5>');
			the_excerpt();
		?>
		
		<a class="text-link" href="<?php the_permalink(); ?>">
			<?php echo get_option('blog_read_more','Read More'); ?>
		</a>
		
	</div>
</div>