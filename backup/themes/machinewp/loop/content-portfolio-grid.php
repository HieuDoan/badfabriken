<div class="col-md-4 col-sm-6 project" data-filter="<?php echo ebor_the_terms('portfolio_category', ' ', 'slug'); ?>">

	<a href="<?php the_permalink(); ?>">
		<div class="background-image-holder fadeIn">
			<?php the_post_thumbnail('medium', array('class' => 'background-image')); ?>
		</div>
	</a>
	
	<?php the_title('<h2>', '</h2>'); ?>
	<span><?php echo ebor_the_terms('portfolio_category', ', ', 'name'); ?></span>
	<?php ebor_the_subtitle('<h5>', '</h5>'); ?>
	
</div><!--end of project-->