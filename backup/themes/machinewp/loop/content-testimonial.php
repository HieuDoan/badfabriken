<li>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<?php
					the_content();
					the_title('<span class="alt-font">', '</span>');
				?>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</li>