<?php
	global $post;
	$job = get_post_meta( $post->ID, '_ebor_the_job_title', true );
	$icons = get_post_meta( $post->ID, '_ebor_team_social_icons', true );
?>

<div class="container">
	<div class="row">
	
		<div class="col-md-5 col-md-offset-1 col-sm-6">
			<?php 
				the_title('<h3>', '</h3><span class="alt-font sub">'. esc_html($job) .'</span>'); 
				the_content();
				
				if( is_array($icons) ){
					echo '<ul class="social-links">';
						foreach( $icons as $key => $icon ){
							if(!( isset( $icon['_ebor_social_icon_url'] ) ))
								continue;
								
							echo '<li><a href="'. esc_url($icon['_ebor_social_icon_url']) .'" target="_blank"><i class="icon '. esc_attr($icon['_ebor_social_icon']) .'"></i></a></li>';
						}
					echo '</ul>';
				}
			?>
		</div>
	
		<div class="col-sm-6 text-center">
			<?php the_post_thumbnail('full'); ?>
		</div>
		
	</div><!--end of row-->
</div><!--end of container-->