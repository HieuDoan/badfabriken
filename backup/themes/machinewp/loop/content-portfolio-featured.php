<section class="hero-project">

	<div class="background-image-holder">
		<?php the_post_thumbnail('full', array('class' => 'background-image')); ?>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-7">
				<div class="text-block">
					<?php 
						the_title('<div class="detail-line"></div><h5 class="text-white">', '</h5>');
						ebor_the_subtitle('<h2 class="text-white">', '</h2>'); 
					?>
				</div>
				<a class="text-link text-white" href="<?php the_permalink(); ?>">View the case study</a>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->

</section>