<?php
	get_header();
	the_post();
	
	$sidebar_active = is_active_sidebar('primary');
	if( 'on' == esc_html(get_post_meta( $post->ID, '_ebor_disable_sidebar', true )) )
		$sidebar_active = false;
		
	$class = ( $sidebar_active ) ? 'col-md-7 col-sm-9' : 'col-md-8 col-sm-9';
	
	if( 'on' == esc_html(get_post_meta( $post->ID, '_ebor_enable_header', true )) ){
		$image = wp_get_attachment_url(get_post_thumbnail_id(),'full');
		ebor_the_page_title_alt($image);
	} else {
		ebor_the_page_title();
	}
?>

<section class="article-wrapper">
	<div class="container">
		<div class="row">
		
			<div class="col-md-2 col-sm-3 text-center">
				<?php echo get_avatar( get_the_author_meta('email'), 120 ); ?>
				<span><?php _e('by','machine'); ?> </span>
				<span class="alt-font uppercase author-name"><?php the_author_posts_link(); ?></span>
			</div>

			<div class="<?php echo esc_attr($class); ?>">
				<div class="article-single">
				
					<div class="text-block">
						<div class="detail-line"></div>
						<h5><?php the_time(get_option('date_format')); ?></h5>
					</div>
					
					<?php
						the_content();
						wp_link_pages();
					?>
		
				</div>
			</div>

			<?php 
				if( $sidebar_active )
					get_sidebar(); 
			?>

		</div><!--end of row-->
	</div><!--end of container-->
</section>		
						
<?php 
	if( comments_open() )
		comments_template();
		
	get_footer();			