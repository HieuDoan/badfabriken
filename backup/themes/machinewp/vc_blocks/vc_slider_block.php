<?php

/**
 * The Shortcode
 */
function ebor_slider_shortcode( $atts, $content = null ) {
	
	extract( 
		shortcode_atts( 
			array(
				'parallax' => '',
				'short' => ''
			), $atts 
		) 
	);
	
	$output = '<section class="hero-slider centered-text-slider '. $parallax .' '. $short .'"><ul class="slides">'. do_shortcode($content) .'</ul></section>';
	return $output;
}
add_shortcode( 'machine_slider', 'ebor_slider_shortcode' );

/**
 * The Shortcode
 */
function ebor_slider_content_shortcode( $atts, $content = null ) {

	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'image' => '',
				'overlay' => '',
				'fullheight' => ''
			), $atts 
		) 
	);
	
	$image = wp_get_attachment_image( $image, 'full', 0, array('class' => 'background-image') );
	
	$output = '<li class="'. esc_attr($overlay).' '. esc_attr($fullheight).'">';
	
	if($image)
		$output .= '<div class="background-image-holder">'. $image .'</div>';
	
	if($content)
		$output .= '<div class="container vertical-align"><div class="row"><div class="col-sm-12 text-center ebor-slider-content">'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'</div></div></div>';
			
	$output .= '</li>';
	
	return $output;
}
add_shortcode( 'machine_slider_content', 'ebor_slider_content_shortcode' );

// Parent Element
function ebor_slider_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
		    'name'                    => __( 'Hero Slider' , 'machine' ),
		    'base'                    => 'machine_slider',
		    'description'             => __( 'Adds an Image Slider', 'machine' ),
		    'as_parent'               => array('only' => 'machine_slider_content'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		    'content_element'         => true,
		    'show_settings_on_create' => false,
		    "js_view" => 'VcColumnView',
		    "category" => __('Machine - WP Theme', 'machine'),
		    'params'          => array(
		        array(
		        	"type" => "dropdown",
		        	"heading" => __("Use parallax scrolling? (headers only)", 'machine'),
		        	"param_name" => "parallax",
		        	"value" => array(
		        		'Yes' => 'parallax',
		        		'No' => ''
		        	),
		        ),
		        array(
		        	"type" => "dropdown",
		        	"heading" => __("Restrict Slider Height?", 'machine'),
		        	"param_name" => "short",
		        	"value" => array(
		        		'No' => '',
		        		'Yes' => 'short-header'
		        	),
		        ),
		    ),
		) 
	);
}
add_action( 'vc_before_init', 'ebor_slider_shortcode_vc' );

// Nested Element
function ebor_slider_content_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
		    'name'            => __('Hero Slider Slide', 'machine'),
		    'base'            => 'machine_slider_content',
		    'description'     => __( 'A slide for the image slider.', 'machine' ),
		    "category" => __('Machine WP Theme', 'machine'),
		    'content_element' => true,
		    'as_child'        => array('only' => 'machine_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
		    'params'          => array(
	            array(
	            	"type" => "attach_image",
	            	"heading" => __("Slide Background Image", 'machine'),
	            	"param_name" => "image"
	            ),
	            array(
	            	"type" => "textarea_html",
	            	"heading" => __("Slide Content", 'machine'),
	            	"param_name" => "content",
	            	'holder' => 'div'
	            ),
	            array(
	            	"type" => "dropdown",
	            	"heading" => __("Darken Image Background?", 'machine'),
	            	"param_name" => "overlay",
	            	"value" => array(
	            		'No' => 'not-overlay',
	            		'Yes' => 'overlay'
	            	),
	            ),
	            array(
	            	"type" => "dropdown",
	            	"heading" => __("Full Window Height Slide?", 'machine'),
	            	"param_name" => "fullheight",
	            	"value" => array(
	            		'No' => 'no',
	            		'Yes' => 'fullscreen-element'
	            	),
	            ),
		    ),
		) 
	);
}
add_action( 'vc_before_init', 'ebor_slider_content_shortcode_vc' );

// A must for container functionality, replace Wbc_Item with your base name from mapping for parent container
if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_machine_slider extends WPBakeryShortCodesContainer {

    }
}

// Replace Wbc_Inner_Item with your base name from mapping for nested element
if(class_exists('WPBakeryShortCode')){
    class WPBakeryShortCode_machine_slider_content extends WPBakeryShortCode {

    }
}