<?php 

/**
 * The Shortcode
 */
function ebor_video_popup_shortcode( $atts ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'video' => ''
			), $atts 
		) 
	);
	
	$output = '<div class="action-strip-2 video-strip">
		<div class="text-center">
			<div class="pre-video">
				<i class="icon pe-7s-play"></i>
				<h2>'.  htmlspecialchars_decode($title) .'</h2>
			</div>
		
			<div class="iframe-holder">
				<i class="close-frame pe-7s-close-circle"></i>
				'. htmlspecialchars_decode(rawurldecode(base64_decode( $video ))) .'
			</div>
		</div>
	</div>';	
	
	return $output;
}
add_shortcode( 'machine_video_popup', 'ebor_video_popup_shortcode' );

/**
 * The VC Functions
 */
function ebor_video_popup_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Video Popup", 'machine'),
			"base" => "machine_video_popup",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
					'value' => 'Watch the video'
				),
				array(
					"type" => "textarea_raw_html",
					"heading" => __("Video iFrame", 'machine'),
					"param_name" => "video",
					'description' => 'e.g: <iframe src="http://player.vimeo.com/video/108018156?color=ffffff?badge=0&amp;title=0&amp;byline=0"></iframe>'
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_video_popup_shortcode_vc' );