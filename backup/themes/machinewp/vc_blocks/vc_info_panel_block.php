<?php 

/**
 * The Shortcode
 */
function ebor_info_panel_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => ''
			), $atts 
		) 
	);
	
	$output ='<div class="info-panel">
		<div class="title">
			<h4>'. htmlspecialchars_decode($title) .'</h4>
		</div>
		<div class="body">
			'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'
		</div>
	</div>';
	
	return $output;
}
add_shortcode( 'machine_info_panel', 'ebor_info_panel_shortcode' );

/**
 * The VC Functions
 */
function ebor_info_panel_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Info Panel", 'machine'),
			"base" => "machine_info_panel",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
					'holder' => 'div'
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					'holder' => 'div'
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_info_panel_shortcode_vc' );