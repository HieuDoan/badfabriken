<?php 

/**
 * The Shortcode
 */
function ebor_team_shortcode( $atts ) {
	extract( 
		shortcode_atts( 
			array(
				'type' => 'large',
				'pppage' => '999',
				'filter' => 'all',
				'type' => 'grid'
			), $atts 
		) 
	);
	
	/**
	 * Initial query args
	 */
	$query_args = array(
		'post_type' => 'team',
		'posts_per_page' => $pppage
	);
	
	if (!( $filter == 'all' )) {
		if( function_exists( 'icl_object_id' ) ){
			$filter = (int)icl_object_id( $filter, 'team_category', true);
		}
		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'team_category',
				'field' => 'id',
				'terms' => $filter
			)
		);
	}
	
	/**
	 * Finally, here's the query.
	 */
	$block_query = new WP_Query( $query_args );
	
	ob_start();
	
	if( 'grid' == $type ) :
?>
	
	<div class="team-2">
		<div class="row">
			<?php 
				if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
					
					/**
					 * Get blog posts by blog layout.
					 */
					get_template_part('loop/content', 'team-grid');
				
				endwhile;	
				else : 
					
					/**
					 * Display no posts message if none are found.
					 */
					get_template_part('loop/content','none');
					
				endif;
				wp_reset_query();
			?>
		</div><!--end of row-->
	</div>
	
<?php elseif( 'featured' == $type ) : ?>
	
	<?php
		if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
			
			$class = ( ($block_query->current_post + 1) % 2 == 0 ) ? 'dark-bg' : '';
			echo '<section class="team-1-single '. esc_attr($class) .'">';
			
			/**
			 * Get blog posts by blog layout.
			 */
			get_template_part('loop/content', 'team-featured');
			
			echo '</section>';
		
		endwhile;	
		else : 
			
			/**
			 * Display no posts message if none are found.
			 */
			get_template_part('loop/content','none');
			
		endif;
		wp_reset_query();
	?>
	
<?php	
	endif;
	
	$output = ob_get_contents();
	ob_end_clean();
	
	return $output;
}
add_shortcode( 'machine_team', 'ebor_team_shortcode' );

/**
 * The VC Functions
 */
function ebor_team_shortcode_vc() {
	
	$portfolio_types = array(
		'Grid Team' => 'grid',
		'Featured Team' => 'featured'
	);
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Team Feed", 'machine'),
			"base" => "machine_team",
			"category" => __('Machine - WP Theme', 'machine'),
			'description' => 'Add your team posts to the page.',
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Show How Many Posts?", 'machine'),
					"param_name" => "pppage",
					"value" => '8'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Display type", 'machine'),
					"param_name" => "type",
					"value" => $portfolio_types
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_team_shortcode_vc');