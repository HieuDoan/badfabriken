<?php 

/**
 * The Shortcode
 */
function ebor_side_icon_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'icon' => ''
			), $atts 
		) 
	);
	
	$output ='<div class="narrow-feature">
				<i class="icon '. esc_attr($icon) .'"></i>
				<div class="content">'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'</div>
			  </div>';
	
	return $output;
}
add_shortcode( 'machine_side_icon', 'ebor_side_icon_shortcode' );

/**
 * The VC Functions
 */
function ebor_side_icon_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Side Icon", 'machine'),
			'description' => 'Block with text and icon on left.',
			"base" => "machine_side_icon",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "ebor_icons",
					"heading" => __("Click an Icon to choose", 'machine'),
					"param_name" => "icon",
					"value" => array_values(ebor_get_icons()),
					'holder' => 'div',
					'description' => 'Type "none" or leave blank to hide icons.'
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					'holder' => 'div'
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_side_icon_shortcode_vc' );