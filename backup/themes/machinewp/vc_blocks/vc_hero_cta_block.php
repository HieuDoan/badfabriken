<?php 

/**
 * The Shortcode
 */
function ebor_hero_cta_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'subtitle' => '',
				'link' => '',
				'button_text' => '',
				'image' => ''
			), $atts 
		) 
	);
	
	ob_start();
	
	$attachments = explode(',', $image);
	
	if(!(is_array($attachments)))
		$attachments[0] = false;
?>
	
	<div class="promo-1">
	
		<div class="promo-image-holder">
			<div class="background-image-holder">
				<?php echo wp_get_attachment_image( $attachments[0], 'full', 0, array('class' => 'background-image') ); ?>
			</div>
		</div>
	
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-sm-7 left-content">
						<div class="vertical-align">
							<?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
						</div>
					</div>
				
					<div class="col-sm-5 right-content">
						<div class="vertical-align">
							<span class="text-white title"><?php echo htmlspecialchars_decode($title); ?></span>
							<span class="sub"><?php echo htmlspecialchars_decode($subtitle); ?></span>
							<a class="btn btn-white" href="<?php echo esc_url($link); ?>"><?php echo htmlspecialchars_decode($button_text); ?></a>
						</div>
					</div>
				</div>
			</div><!--end of row-->
		</div><!--end of container-->
		
	</div>
	
<?php
	$output = ob_get_contents();
	ob_end_clean();
	
	return $output;
}
add_shortcode( 'machine_hero_cta', 'ebor_hero_cta_shortcode' );

/**
 * The VC Functions
 */
function ebor_hero_cta_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Hero Call To Action", 'machine'),
			"base" => "machine_hero_cta",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "attach_image",
					"heading" => __("Background Image", 'machine'),
					"param_name" => "image",
				),
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title"
				),
				array(
					"type" => "textfield",
					"heading" => __("Subtitle", 'machine'),
					"param_name" => "subtitle"
				),
				array(
					"type" => "textfield",
					"heading" => __("Button URL", 'machine'),
					"param_name" => "link"
				),
				array(
					"type" => "textfield",
					"heading" => __("Button Text", 'machine'),
					"param_name" => "button_text"
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					"value" => '',
					'holder' => 'div'
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_hero_cta_shortcode_vc' );