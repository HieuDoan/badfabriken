<?php 

/**
 * The Shortcode
 */
function ebor_icon_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'icon' => '',
				'alignment' => ''
			), $atts 
		) 
	);
	
	$output = false;
		
	if( $icon == 'none' || '' == $icon )
		return $output;
	
	$output .= '<div class="'. $alignment .' feature-lists"><i class="icon icon-large '. $icon .'"></i></div>';
	
	return $output;
}
add_shortcode( 'machine_icon', 'ebor_icon_shortcode' );

/**
 * The VC Functions
 */
function ebor_icon_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Machine Icons", 'machine'),
			"base" => "machine_icon",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "ebor_icons",
					"heading" => __("Click an Icon to choose", 'machine'),
					"param_name" => "icon",
					"value" => array_values(ebor_get_icons()),
					'holder' => 'div',
					'description' => 'Type "none" or leave blank to hide icons.'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Icon Alignment", 'machine'),
					"param_name" => "alignment",
					"value" => array(
						'Left' => '',
						'Center' => 'text-center'
					)
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_icon_shortcode_vc' );