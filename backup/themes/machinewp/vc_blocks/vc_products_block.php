<?php 

/**
 * The Shortcode
 */
function ebor_products_shortcode( $atts ) {
	extract( 
		shortcode_atts( 
			array(
				'pppage' => '999',
				'filter' => 'all'
			), $atts 
		) 
	);
	
	// Fix for pagination
	if( is_front_page() ) { 
		$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1; 
	} else { 
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	}
	
	/**
	 * Setup post query
	 */
	$query_args = array(
		'post_type' => 'product',
		'posts_per_page' => $pppage,
		'paged' => $paged
	);
	
	if (!( $filter == 'all' )) {
		if( function_exists( 'icl_object_id' ) ){
			$filter = (int)icl_object_id( $filter, 'product_cat', true);
		}
		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'id',
				'terms' => $filter
			)
		);
	}
	
	/**
	 * Finally, here's the query.
	 */
	$block_query = new WP_Query( $query_args );
	
	ob_start();
?>
	
		<?php if ( $block_query->have_posts() ) : ?>
			
			<div class="row">
				<div class="upper-meta">
					
					<div class="col-sm-6">
						<span class="paging-detail">
							<?php
								$paged    = max( 1, $block_query->get( 'paged' ) );
								$per_page = $block_query->get( 'posts_per_page' );
								$total    = $block_query->found_posts;
								$first    = ( $per_page * $paged ) - $per_page + 1;
								$last     = min( $total, $block_query->get( 'posts_per_page' ) * $paged );
							
								if ( 1 == $total ) {
									_e( 'Showing the single result', 'machine' );
								} elseif ( $total <= $per_page || -1 == $per_page ) {
									printf( __( 'Showing all %d results', 'machine' ), $total );
								} else {
									printf( _x( 'Showing %1$d&ndash;%2$d of %3$d results', '%1$d = first, %2$d = last, %3$d = total', 'machine' ), $first, $last, $total );
								}
								?>
						</span>
					</div>
						
				</div>
			</div>
								
			<div class="row">

					<?php woocommerce_product_loop_start(); ?>
		
						<?php woocommerce_product_subcategories(); ?>
		
						<?php while ( $block_query->have_posts() ) : $block_query->the_post(); ?>
		
							<?php wc_get_template_part( 'content', 'product' ); ?>
		
						<?php endwhile; // end of the loop. ?>
		
					<?php woocommerce_product_loop_end(); ?>
		
					<?php
						/**
						* Post pagination, use ebor_pagination() first and fall back to default
						*/
						echo function_exists('ebor_pagination') ? ebor_pagination($block_query->max_num_pages) : posts_nav_link();
					?>

			</div>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; wp_reset_query(); ?>
			
<?php	
	$output = ob_get_contents();
	ob_end_clean();
	
	return $output;
}
add_shortcode( 'machine_products', 'ebor_products_shortcode' );

/**
 * The VC Functions
 */
function ebor_products_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("WooCommerce Products", 'machine'),
			"base" => "machine_products",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Show How Many Posts?", 'machine'),
					"param_name" => "pppage",
					"value" => '8'
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_products_shortcode_vc');