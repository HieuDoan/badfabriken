<?php 

/**
 * The Shortcode
 */
function ebor_blog_shortcode( $atts ) {
	extract( 
		shortcode_atts( 
			array(
				'type' => 'grid',
				'pppage' => '10',
				'filter' => 'all'
			), $atts 
		) 
	);
	
	// Fix for pagination
	if( is_front_page() ) { 
		$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1; 
	} else { 
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	}
	
	/**
	 * Setup post query
	 */
	$query_args = array(
		'post_type' => 'post',
		'posts_per_page' => $pppage,
		'paged' => $paged
	);
	
	if (!( $filter == 'all' )) {
		if( function_exists( 'icl_object_id' ) ){
			$filter = (int)icl_object_id( $filter, 'category', true);
		}
		$query_args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field' => 'id',
				'terms' => $filter
			)
		);
	}
	
	/**
	 * Finally, here's the query.
	 */
	$block_query = new WP_Query( $query_args );
	
	ob_start();
?>
	
	<?php if( 'grid' == $type ) : ?>
		
		<div class="blog-snippets-1">
		
			<div class="row">
				<?php 
					if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
						
						/**
						 * Get blog posts by blog layout.
						 */
						get_template_part('loop/content', 'blog-grid');
					
					endwhile;	
					else : 
						
						/**
						 * Display no posts message if none are found.
						 */
						get_template_part('loop/content','none');
						
					endif;
					wp_reset_query();
				?>	
			</div>
			
			<?php
				/**
				* Post pagination, use ebor_pagination() first and fall back to default
				*/
				echo function_exists('ebor_pagination') ? ebor_pagination($block_query->max_num_pages) : posts_nav_link();
			?>
			
		</div>	
		
	<?php elseif( 'grid-sidebar' == $type ) : ?>
		
		<div class="blog-snippets-1">
		
			<div class="row">
			
				<div class="col-md-9">
					<div class="row">
						<?php 
							if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
								
								/**
								 * Get blog posts by blog layout.
								 */
								get_template_part('loop/content', 'blog-grid-sidebar');
							
							endwhile;	
							else : 
								
								/**
								 * Display no posts message if none are found.
								 */
								get_template_part('loop/content','none');
								
							endif;
							wp_reset_query();
						?>	
					</div>
				</div>
			
				<?php 
					if( is_active_sidebar('primary') )
						get_sidebar(); 
				?>
			
			</div><!--end of row-->
			
			<?php
				/**
				* Post pagination, use ebor_pagination() first and fall back to default
				*/
				echo function_exists('ebor_pagination') ? ebor_pagination($block_query->max_num_pages) : posts_nav_link();
			?>
			
		</div>	
		
	<?php elseif( 'classic' == $type ) : ?>
		
		<section class="blog-snippets-2">
			<div class="container">
			
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<?php 
							if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
								
								/**
								 * Get blog posts by blog layout.
								 */
								get_template_part('loop/content', 'blog-classic');
							
							endwhile;	
							else : 
								
								/**
								 * Display no posts message if none are found.
								 */
								get_template_part('loop/content','none');
								
							endif;
							wp_reset_query();
						?>
					</div>
				</div><!--end of row-->
			
				<?php
					/**
					* Post pagination, use ebor_pagination() first and fall back to default
					*/
					echo function_exists('ebor_pagination') ? ebor_pagination($block_query->max_num_pages) : posts_nav_link();
				?>
			
			</div><!--end of container-->
		</section>
		
	<?php elseif( 'classic-sidebar' == $type ) : ?>
		
		<section class="blog-snippets-2 full-blog-snippets-2">
			<div class="container">
			
				<div class="row">
				
					<div class="col-md-8">
						<div class="col-xs-12">
							<?php 
								if ( $block_query->have_posts() ) : while ( $block_query->have_posts() ) : $block_query->the_post();
									
									/**
									 * Get blog posts by blog layout.
									 */
									get_template_part('loop/content', 'blog-classic');
								
								endwhile;	
								else : 
									
									/**
									 * Display no posts message if none are found.
									 */
									get_template_part('loop/content','none');
									
								endif;
								wp_reset_query();
							?>
						</div>
					</div>
					
					<div class="col-md-1"></div>
				
					<?php 
						if( is_active_sidebar('primary') )
							get_sidebar();  
					?>
					
				</div><!--end of row-->
			
				<?php
					/**
					* Post pagination, use ebor_pagination() first and fall back to default
					*/
					echo function_exists('ebor_pagination') ? ebor_pagination($block_query->max_num_pages) : posts_nav_link();
				?>
			
			</div><!--end of container-->
		</section>	
		
	<?php endif; ?>
			
<?php	
	$output = ob_get_contents();
	ob_end_clean();
	
	return $output;
}
add_shortcode( 'machine_blog', 'ebor_blog_shortcode' );

/**
 * The VC Functions
 */
function ebor_blog_shortcode_vc() {
	
	$blog_types = array(
		'Grid Blog' => 'grid',
		'Grid Blog Sidebar' => 'grid-sidebar',
		'Classic Blog' => 'classic',
		'Classic Blog Sidebar' => 'classic-sidebar'
	);
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Blog", 'machine'),
			"base" => "machine_blog",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Show How Many Posts?", 'machine'),
					"param_name" => "pppage",
					"value" => '8'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Display type", 'machine'),
					"param_name" => "type",
					"value" => $blog_types
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_blog_shortcode_vc');