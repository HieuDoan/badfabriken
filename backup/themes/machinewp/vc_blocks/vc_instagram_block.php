<?php 

/**
 * The Shortcode
 */
function ebor_instagram_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => ''
			), $atts 
		) 
	);
	
	$output = '<section class="instagram-1">
		<div class="instafeed" data-user-name="'. esc_attr($title) .'">
			<ul></ul>
		</div>
	
		<div class="container vertical-align">
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="#">
						<h2><i class="icon social_instagram"></i> '. esc_html($title) .'</h2><br>
						'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'
					</a>
				</div>
			</div><!--end of row-->
		</div><!--end of container-->
	</section>';
	
	return $output;
}
add_shortcode( 'machine_instagram', 'ebor_instagram_shortcode' );

/**
 * The VC Functions
 */
function ebor_instagram_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Instagram Feed", 'machine'),
			"base" => "machine_instagram",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Instagram Username", 'machine'),
					"param_name" => "title",
					"value" => '',
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content Under Feed", 'machine'),
					"param_name" => "content",
					"value" => '',
					"description" => '',
					'holder' => 'div'
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_instagram_shortcode_vc' );