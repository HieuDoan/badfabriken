<?php 

/**
 * The Shortcode
 */
function ebor_hero_video_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'image' => '',
				'mpfour' => '',
				'ogv' => '',
				'webm' => '',
			), $atts 
		) 
	);
	
	$output = false;
	
	ob_start();
	
	//images
	$attachments = explode(',', $image);
	
	if(is_array($attachments)) :
?>
	
	<section class="hero-slider large-image fixed-header">
		<ul class="slides">
			<li>
				<div class="background-image-holder">
					<?php echo wp_get_attachment_image( $attachments[0], 'full', 0, array('class' => 'background-image') ); ?>
				</div>
			
				<div class="video-holder gradient-overlay video-wrapper">
					<video autoplay="" muted="" loop="">
					  <source src="<?php echo $webm; ?>" type="video/webm">
					  <source src="<?php echo $mpfour; ?>" type="video/mp4">
					  <source src="<?php echo $ogv; ?>" type="video/ogg">	
					</video>
				</div>	
	
				<div class="container vertical-align">
					<div class="row">
						<div class="col-sm-12 ebor-slider-content">
							<?php echo wpautop(do_shortcode(htmlspecialchars_decode($content))); ?>
						</div>
					</div>
				</div><!--end of container-->	
			</li>
		
		</ul>
	</section>

<?php
	endif;
	
	$output = ob_get_contents();
	ob_end_clean();
	
	return $output;
}
add_shortcode( 'machine_hero_video', 'ebor_hero_video_shortcode' );

/**
 * The VC Functions
 */
function ebor_hero_video_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Hero Video", 'machine'),
			"base" => "machine_hero_video",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "attach_image",
					"heading" => __("Fallback Image", 'machine'),
					"param_name" => "image",
					"value" => '',
					"description" => __('Add a fallback image for mobile devices (required)', 'machine')
				),
				array(
					"type" => "textfield",
					"heading" => __("Self Hosted Video Background?, .webm extension", 'machine'),
					"param_name" => "webm",
					"value" => '',
					"description" => __('Please fill all extensions', 'machine')
				),
				array(
					"type" => "textfield",
					"heading" => __("Self Hosted Video Background?, .mp4 extension", 'machine'),
					"param_name" => "mpfour",
					"value" => '',
					"description" => __('Please fill all extensions', 'machine')
				),
				array(
					"type" => "textfield",
					"heading" => __("Self Hosted Video Background?, .ogv extension", 'machine'),
					"param_name" => "ogv",
					"value" => '',
					"description" => __('Please fill all extensions', 'machine')
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					"value" => '',
					'holder' => 'div'
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_hero_video_shortcode_vc' );