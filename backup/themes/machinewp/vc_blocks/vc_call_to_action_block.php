<?php 

/**
 * The Shortcode
 */
function ebor_call_to_action_shortcode( $atts ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'subtitle' => '',
				'link' => '',
				'button_text' => '',
				'icon' => 'pe-7s-look'
			), $atts 
		) 
	);
	
	$output = '<div class="action-strip-1"><div class="text-center">';
	
	if($title)		
		$output .= '<span class="alt-font">'.  htmlspecialchars_decode($title) .'</span>';
		
	if($subtitle)		
		$output .= '<span class="title">'.  htmlspecialchars_decode($subtitle) .'</span>';
		
	$output .= '<i class="icon '. esc_attr($icon) .'"></i>
				<a class="text-link" href="'. esc_url($link) .'" target="_blank">'. htmlspecialchars_decode($button_text) .' <i class="icon arrow_right"></i></a>
			</div>
		</div>';
	
	return $output;
}
add_shortcode( 'machine_call_to_action', 'ebor_call_to_action_shortcode' );

/**
 * The VC Functions
 */
function ebor_call_to_action_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Call To Action", 'machine'),
			"base" => "machine_call_to_action",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "ebor_icons",
					"heading" => __("Click an Icon to choose", 'machine'),
					"param_name" => "icon",
					"value" => array_values(ebor_get_icons()),
					'holder' => 'div',
					'description' => 'Type "none" or leave blank to hide icons.'
				),
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title"
				),
				array(
					"type" => "textfield",
					"heading" => __("Subtitle", 'machine'),
					"param_name" => "subtitle"
				),
				array(
					"type" => "textfield",
					"heading" => __("Button URL", 'machine'),
					"param_name" => "link"
				),
				array(
					"type" => "textfield",
					"heading" => __("Button Text", 'machine'),
					"param_name" => "button_text"
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_call_to_action_shortcode_vc' );