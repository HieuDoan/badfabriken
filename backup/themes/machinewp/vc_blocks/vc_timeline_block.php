<?php 

/**
 * The Shortcode
 */
function ebor_timeline_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'icon' => '',
				'align' => 'left-content'
			), $atts 
		) 
	);
	
	$second_align = ( 'left-content' == $align ) ? 'right-content' : 'left-content';
	
	$output = '
	<div class="timeline-section">
		<div class="'. esc_attr($align) .' timeline-content">
			'. do_shortcode(htmlspecialchars_decode($content)) .'
		</div>
	
		<div class="'. esc_attr($second_align) .' timeline-content">
			<i class="'. esc_attr($icon) .'"></i>
		</div>
	</div>
	';
	
	return $output;
}
add_shortcode( 'machine_timeline', 'ebor_timeline_shortcode' );

/**
 * The VC Functions
 */
function ebor_timeline_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Timeline", 'machine'),
			'description' => 'Timeline block with icon',
			"base" => "machine_timeline",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "ebor_icons",
					"heading" => __("Click an Icon to choose", 'machine'),
					"param_name" => "icon",
					"value" => array_values(ebor_get_icons()),
					'holder' => 'div',
					'description' => 'Type "none" or leave blank to hide icons.'
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					'holder' => 'div'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Display Alignment", 'machine'),
					"param_name" => "align",
					"value" => array_flip(array(
						'left-content' => 'Content Left',
						'right-content' => 'Content Right'
					))
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_timeline_shortcode_vc' );