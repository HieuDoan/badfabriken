<?php 

/**
 * The Shortcode
 */
function ebor_pricing_table_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'text' => '',
				'currency' => '$',
				'amount' => '3',
				'button_url' => '',
				'detail' => '',
				'feature' => 'no',
				'layout' => 'standard',
				'small' => ''
			), $atts 
		) 
	);
	
	$lines = explode( ',', $text );
	
	ob_start();
	
	if( 'standard' == $layout ) :
?>
					
	<div class="pricing-tables-2">
		<div class="pricing-table-2">
			<h2 class="uppercase"><?php echo htmlspecialchars_decode($title); ?></h2>
			<ul>
				<?php 
					foreach( $lines as $line ){
						echo '<li>' . htmlspecialchars_decode($line) . '</li>';
					}
				?>
			</ul>
			<span class="price"><?php echo htmlspecialchars_decode($currency); echo htmlspecialchars_decode($amount); ?></span>
			<div class="action">
				<a href="<?php echo esc_url($button_url); ?>">
					<span class="alt-font uppercase"><?php echo htmlspecialchars_decode($detail); ?></span>
				</a>
			</div>
		</div>
	</div>
	
<?php else : ?>
	
	<div class="pricing-1">
		<div class="pricing-table-1">
			<div class="pricing-header">
				<h5 class="text-white"><?php echo htmlspecialchars_decode($title); ?></h5>
				<div class="price">
					<span class="dollar text-white"><?php echo htmlspecialchars_decode($currency); ?></span>
					<span class="amount text-white"><?php echo htmlspecialchars_decode($amount); ?></span>
					<span class="alt-font uppercase text-white"><?php echo htmlspecialchars_decode($small); ?></span>
				</div>
			</div>
			<div class="pricing-details">
				<ul>
					<?php 
						foreach( $lines as $line ){
							echo '<li><i class="icon pe-7s-check text-white"></i><span class="text-white">' . htmlspecialchars_decode($line) . '</span></li>';
						}
					?>
				</ul>
				<a class="text-link text-white" href="<?php echo esc_url($button_url); ?>"><?php echo htmlspecialchars_decode($detail); ?> <i class="icon arrow_right"></i></a>
			</div>
		</div>
	</div>
			
<?php
	endif; 
	
	$output = ob_get_contents();
	ob_end_clean();

	return $output;
}
add_shortcode( 'machine_pricing_table', 'ebor_pricing_table_shortcode' );

/**
 * The VC Functions
 */
function ebor_pricing_table_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Pricing table", 'machine'),
			"base" => "machine_pricing_table",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Button URL", 'machine'),
					"param_name" => "button_url",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => __("Currency", 'machine'),
					"param_name" => "currency",
					"value" => '$',
				),
				array(
					"type" => "textfield",
					"heading" => __("Amount", 'machine'),
					"param_name" => "amount",
					"value" => '3',
				),
				array(
					"type" => "textfield",
					"heading" => __("Button Text", 'machine'),
					"param_name" => "detail",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
					"value" => '',
					'holder' => 'div'
				),
				array(
					"type" => "exploded_textarea",
					"heading" => __("Pricing details, one per line", 'machine'),
					"param_name" => "text",
					"value" => '',
				),
				array(
					"type" => "dropdown",
					"heading" => __("Pricing Table Layout", 'machine'),
					"param_name" => "layout",
					"value" => array_flip(array(
						'standard' => 'Standard Pricing Table',
						'dropdown' => 'Dropdown Table'
					))
				),
				array(
					"type" => "textfield",
					"heading" => __("Small Text", 'machine'),
					"param_name" => "small",
					"value" => '',
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_pricing_table_shortcode_vc' );