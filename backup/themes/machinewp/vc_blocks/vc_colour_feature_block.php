<?php 

/**
 * The Shortcode
 */
function ebor_colour_feature_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'icon' => '',
				'layout' => 'big-icon'
			), $atts 
		) 
	);
	
	if( 'big-icon' == $layout ){
		
		$output ='<div class="color-feature">
			<div class="detail-line"></div>
			<h5 class="alt-font text-white">'. htmlspecialchars_decode($title) .'</h5>
			<i class="'. $icon .'"></i>
			<div class="block-content">'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'</div>
		</div>';
		
	} else {
	
		$output = '<div class="fullwidth-feature">
			<div class="top">
				<i class="'. $icon .'"></i>
				<h5>'. htmlspecialchars_decode($title) .'</h5>
				<div class="detail-line"></div>
				<div class="block-content">'. wpautop(do_shortcode(htmlspecialchars_decode($content))) .'</div>
			</div>
		</div>';
	
	}
	
	return $output;
}
add_shortcode( 'machine_colour_feature', 'ebor_colour_feature_shortcode' );

/**
 * The VC Functions
 */
function ebor_colour_feature_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Feature Block", 'machine'),
			"base" => "machine_colour_feature",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
				),
				array(
					"type" => "ebor_icons",
					"heading" => __("Click an Icon to choose", 'machine'),
					"param_name" => "icon",
					"value" => array_values(ebor_get_icons()),
					'holder' => 'div',
					'description' => 'Type "none" or leave blank to hide icons.'
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					"value" => '',
					'holder' => 'div'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Display type", 'machine'),
					"param_name" => "layout",
					"value" => array(
						'Big Icon' => 'big-icon',
						'Small Icon & Hover' => 'small-icon'
					)
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_colour_feature_shortcode_vc' );