<?php 

/**
 * The Shortcode
 */
function ebor_section_title_shortcode( $atts ) {	
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'subtitle' => ''
			), $atts 
		) 
	);
	
	$output = '<div class="text-block"><div class="detail-line"></div>';
	
	if( $subtitle )
		$output .= '<h5>'. htmlspecialchars_decode($subtitle) .'</h5>';
		
	if( $title )
		$output .= '<h4>'. htmlspecialchars_decode($title) .'</h4>';
		
	$output .= '</div>';
	
	return $output;
}
add_shortcode( 'machine_section_title', 'ebor_section_title_shortcode' );

/**
 * The VC Functions
 */
function ebor_section_title_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Section Title", 'machine'),
			"base" => "machine_section_title",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Subtitle", 'machine'),
					"param_name" => "subtitle",
					'holder' => 'div'
				),
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
					'holder' => 'div'
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_section_title_shortcode_vc' );