<?php 

/**
 * The Shortcode
 */
function ebor_feature_box_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'link' => '',
				'image' => '',
				'button_text' => '',
				'sub' => '',
				'target' => '_self',
				'layout' => 'feature'
			), $atts 
		) 
	);
	
	$attachments = explode(',', $image);
	
	if(!(is_array($attachments)))
		$attachments[0] = false;
	
	if( 'feature' == $layout ){
		
		$output = '
			<div class="feature-box">
				<div class="background-image-holder overlay-light-gradient">'. wp_get_attachment_image( $attachments[0], 'full', 0, array('class' => 'background-image') ) .'</div>
				<div class="content">'. do_shortcode(htmlspecialchars_decode($content)) .'</div>
			</div>
		';
	
	} else {
		
		$output = '
			<div class="image-tile">
				<div class="background-image-holder overlay">
					'. wp_get_attachment_image( $attachments[0], 'full', 0, array('class' => 'background-image') ) .'
				</div>
				<div class="content">
					'. do_shortcode(htmlspecialchars_decode($content)) .'
				</div>
			</div>
		';
	
	}

	return $output;
}
add_shortcode( 'machine_feature_box', 'ebor_feature_box_shortcode' );

/**
 * The VC Functions
 */
function ebor_feature_box_shortcode_vc() {
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Feature Box", 'machine'),
			"base" => "machine_feature_box",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "attach_image",
					"heading" => __("Background Image", 'machine'),
					"param_name" => "image",
					"value" => '',
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Content", 'machine'),
					"param_name" => "content",
					"value" => '',
					'holder' => 'div'
				),
				array(
					"type" => "dropdown",
					"heading" => __("Feature Box Layout", 'machine'),
					"param_name" => "layout",
					"value" => array_flip(array(
						'feature' => 'Big Feature Box',
						'quick' => 'Small Box'
					))
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'ebor_feature_box_shortcode_vc' );