<?php 

/**
 * The Shortcode
 */
function ebor_stat_shortcode( $atts, $content = null ) {
	extract( 
		shortcode_atts( 
			array(
				'title' => '',
				'subtitle' => ''
			), $atts 
		) 
	);
	
	$output ='<div class="stats-large text-center">
				<div class="stat">
					<span class="number text-white">'. htmlspecialchars_decode($title) .'</span>
					<h2 class="text-white">'. htmlspecialchars_decode($subtitle) .'</h2>
					<p class="lead text-white">
						'. htmlspecialchars_decode($content) .'
					</p>
				</div><!--end of individual stat-->
			</div>';
	
	return $output;
}
add_shortcode( 'machine_stat', 'ebor_stat_shortcode' );

/**
 * The VC Functions
 */
function ebor_stat_shortcode_vc() {
	
	vc_map( 
		array(
			"icon" => 'machine-vc-block',
			"name" => __("Stat Block", 'machine'),
			"base" => "machine_stat",
			"category" => __('Machine - WP Theme', 'machine'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => __("Title", 'machine'),
					"param_name" => "title",
					'holder' => 'div'
				),
				array(
					"type" => "textfield",
					"heading" => __("Subtitle", 'machine'),
					"param_name" => "subtitle"
				),
				array(
					"type" => "textarea",
					"heading" => __("Content", 'machine'),
					"param_name" => "content"
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'ebor_stat_shortcode_vc' );