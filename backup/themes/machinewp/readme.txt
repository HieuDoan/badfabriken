===================================================================
April 6th 2018 - v2.0.14
-------------------------------------------------------------------

* FIXED: Broken Instagram feeds due to unannounced API change

===================================================================
April 2nd 2018 - v2.0.13
-------------------------------------------------------------------

* FIXED: Radio button margin
* FIXED: Broken twitter feeds
* FIXED: Deprecated WooCommerce Function

===================================================================
May 19th 2017 - v2.0.12
-------------------------------------------------------------------

* FIXED: Twitter feeds
* FIXED: Google font string in theme options accepts + now
* UPDATED: Visual composer
* FIXED: Visual composer issues with latest version
* WOOCOMMERCE: 3.0.x integration
* WOOCOMMERCE: Image gallery changed to new WooCommerce zoom gallery
* WOOCOMMERCE: Included template version numbers updated