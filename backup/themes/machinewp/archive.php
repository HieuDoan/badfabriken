<?php 
	get_header(); 
	$term = get_queried_object();
	$background = get_option('blog_header');
	echo ebor_archive_header( $term->name, $background );
	
	get_template_part('loop/loop-blog', get_option('blog_layout', 'classic-sidebar'));

	get_footer();				