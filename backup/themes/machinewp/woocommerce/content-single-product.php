<?php 
	/**
	 * @author  TommusRhodus
	 * @version 9.9.9
	 */
?>
<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="row">
		<div class="col-md-6">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>
		</div>
	
		<div class="col-md-6">
			<div class="product-details summary entry-summary">
	
				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 15
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action( 'woocommerce_single_product_summary' );
	
					/**
					 * woocommerce_after_single_product_summary hook
					 *
					 * @hooked woocommerce_output_product_data_tabs - 10
					 * @hooked woocommerce_template_single_add_to_cart - 11
					 * @hooked woocommerce_template_single_meta - 12
					 * @hooked woocommerce_upsell_display - 15
					 */
					do_action( 'woocommerce_after_single_product_summary' );
				?>
			
				<meta itemprop="url" content="<?php the_permalink(); ?>" />
		
			</div>
		</div><!-- .summary -->
	</div>
	
	<?php woocommerce_output_related_products(); ?>

</div><!-- #product-<?php the_ID(); ?> -->
	
<?php do_action( 'woocommerce_after_single_product' ); ?>
