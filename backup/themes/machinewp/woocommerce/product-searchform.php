<?php 
	/**
	 * @author  TommusRhodus
	 * @version 9.9.9
	 */
?>
<div class="search-bar">
	<form class="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
		<input type="text" id="s2" name="s" placeholder="" />
		<input type="submit" value="" />
		<input type="hidden" name="post_type" value="product" />
		<i class="pe-7s-search"></i>
	</form>
</div>