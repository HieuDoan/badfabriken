<?php 
	/**
	 * @author  TommusRhodus
	 * @version 9.9.9
	 */
?>
<?php

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<ul class="expanding-list">
		<?php foreach ( $tabs as $key => $tab ) : ?>

			<li>
				<span class="title"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?></span>
				<div class="content">
					<?php call_user_func( $tab['callback'], $key, $tab ) ?>
				</div>
			</li>

		<?php endforeach; ?>
	</ul>

<?php endif; ?>
