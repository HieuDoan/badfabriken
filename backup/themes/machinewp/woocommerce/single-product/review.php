<?php 
	/**
	 * @author  TommusRhodus
	 * @version 9.9.9
	 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );
?>

<div class="review">
	<?php echo ebor_rating_html(false, $rating); ?>
	<span class="author"><?php comment_author(); ?></span>
	<?php if ( $comment->comment_approved == '0' ) : ?>
	
		<p><em><?php _e( 'Your comment is awaiting approval', 'machine' ); ?></em></p>

	<?php else : ?>

		<?php comment_text(); if ( get_option( 'woocommerce_review_rating_verification_label' ) === 'yes' )
			if ( wc_customer_bought_product( $comment->comment_author_email, $comment->user_id, $comment->comment_post_ID ) )
				echo '<em class="verified">(' . __( 'verified owner', 'machine' ) . ')</em> '; ?>

	<?php endif; ?>
	
</div>