<?php 
	/**
	 * @author  TommusRhodus
	 * @version 9.9.9
	 */
?>
<?php if( is_singular('product') ) : ?>
<section class="product-single-1 large-pad">
	<div class="container">
<?php else : ?>
<section class="products-1 large-pad">
	<div class="container">
<?php endif;