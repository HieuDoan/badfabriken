<?php 
	get_header(); 
	
	$background = get_option('blog_header');
	echo ebor_archive_header( get_option('blog_title', 'Our Blog'), $background );
	
	get_template_part('loop/loop-blog', get_option('blog_layout', 'classic-sidebar'));

	get_footer();				