<?php get_header(); ?>
					
<section class="error-page fullscreen-element">
	<div class="container vertical-align">
		<div class="row">
			<div class="col-sm-12 text-center">
				<i class="pe-7s-help2"></i>
				<h1 class="text-white"><?php _e('404 - Man overboard!', 'machine'); ?></h1>
				<p class="text-white super-lead"><?php _e('Fear not, help is at hand.', 'machine'); ?></p>
				<a href="<?php echo esc_url(home_url('/')); ?>" class="btn btn-white"><?php _e('Back home', 'machine'); ?></a>
			</div>
		</div><!--end of row-->
	</div><!--end of container-->
</section>

<?php get_footer();