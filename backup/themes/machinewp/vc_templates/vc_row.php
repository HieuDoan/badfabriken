<?php
$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = '';
extract(shortcode_atts(array(
    'el_class'        => '',
    'bg_image'        => '',
    'bg_color'        => '',
    'bg_image_repeat' => '',
    'font_color'      => '',
    'padding'         => '',
    'margin_bottom'   => '',
    'css' => '',
    'background_style' => '',
    'single_link' => '',
    'single_link' => '',
    'mpfour' => '',
    'ogv' => '',
    'webm' => '',
    'vimeo' => '',
    'color_style' => 'light-wrapper',
    'machine_padding' => '',
    'instagram' => '',
    'parallax' => ''
), $atts));

wp_enqueue_script( 'wpb_composer_front_js' );

//Capture background image if set
preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $css, $image);
if(!( isset($image[0][0]) ))
	$image[0][0] = false;

/**
 * Check for scroll id
 */
if($single_link)
	$output .= '<a id="'. ebor_sanitize_title($single_link) .'" class="in-page-link" href="#"></a>';

/**
 * Machine specific output
 */
if( 'full' == $background_style ){
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	
	$output .= '<section class="'. esc_attr($color_style) .' '. $background_style .' '.$css_class.' no-pad"'.$style.'>'. wpb_js_remove_wpautop($content) .'</section>'. $this->endBlockComment('row');
	
} elseif ( 'mega-menu' == $background_style ){
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	
	$output .= wpb_js_remove_wpautop($content) . $this->endBlockComment('row');
	
} elseif( 'image-right' == $background_style ) {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . ' ', $this->settings['base'], $atts );
	
	$output .= '<section class="image-block '. esc_attr($color_style) .' '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"><div class="container"><div class="row"><div class="col-md-5 col-sm-7">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></div>';
	$output .= '<div class="image-holder col-md-6 col-sm-4 pull-right">
		<div class="background-image-holder"><img alt="Slide Background" class="background-image" src="'. $image[0][0] .'"></div>
	</div>';
	$output .= '</section>'.$this->endBlockComment('row');
	
} elseif( 'image-left' == $background_style ) {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . ' ', $this->settings['base'], $atts );
	
	$output .= '<section class="image-block '. esc_attr($color_style) .' '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"><div class="container"><div class="row"><div class="col-md-5 col-sm-7 col-md-offset-7 col-sm-offset-5">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></div>';
	$output .= '<div class="image-holder col-md-6 col-sm-4 pull-left">
		<div class="background-image-holder"><img alt="Slide Background" class="background-image" src="'. $image[0][0] .'"></div>
	</div>';
	$output .= '</section>'.$this->endBlockComment('row');
	
} elseif( 'vimeo-right' == $background_style ) {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . ' ', $this->settings['base'], $atts );
	
	$output .= '<section class="image-block '. esc_attr($color_style) .' '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"><div class="container"><div class="row"><div class="col-md-5 col-sm-7">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></div>';
	$output .= '<div class="image-holder embed-holder col-md-6 col-sm-4 pull-right">
		'. htmlspecialchars_decode(rawurldecode(base64_decode( $vimeo ))) .'
	</div>';
	$output .= '</section>'.$this->endBlockComment('row');
	
} elseif( 'image-full' == $background_style && 'content-moving' == $parallax || 'image-full' == $background_style && 'content-moving-fade' == $parallax  ) {
	
	wp_enqueue_script( 'vc_jquery_skrollr_js' );
	$output .= '<div data-vc-parallax="2.5" class="image-wrapper overlay dark-action vc_row wpb_row vc_row-fluid light-wrapper vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving" data-vc-parallax-image="'. $image[0][0] .'"><div class="container"><div class="row">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></div>'.$this->endBlockComment('row');
	
} elseif( 'image-full' == $background_style ) {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . ' ', $this->settings['base'], $atts );
	
	$output .= '<section class="image-wrapper overlay dark-action '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"><div class="background-image-holder overlay"><img alt="Slide Background" class="background-image" src="'. $image[0][0] .'"></div><div class="container"><div class="row">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></section>'.$this->endBlockComment('row');
	
} elseif( 'instagram-right' == $background_style ) {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . ' ', $this->settings['base'], $atts );
	
	$output .= '<section class="image-block contact-4 '. esc_attr($color_style) .' '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"><div class="container"><div class="row"><div class="col-md-5 col-sm-7">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></div>';
	$output .= '<div class="image-holder col-md-6 col-sm-4 pull-right">
		<div class="instafeed" data-user-name="'. esc_attr($instagram) .'">
			<ul></ul>
		</div>
	</div>';
	$output .= '</section>'.$this->endBlockComment('row');
	
} else {
	
	$el_class = $this->getExtraClass($el_class);
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
	$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);
	
	$output .= '<section class="'. esc_attr($color_style) .' '. $background_style .' '.$css_class.' '. esc_attr($machine_padding) .'"'.$style.'><div class="container"><div class="row">';
	$output .= wpb_js_remove_wpautop($content);
	$output .= '</div></div></section>'.$this->endBlockComment('row');
	
}

echo $output;