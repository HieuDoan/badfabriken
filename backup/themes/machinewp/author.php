<?php 
	get_header(); 
	$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	$background = get_option('blog_header');
	echo ebor_archive_header( __('Posts By: ','machine') . $author->display_name, $background );
	
	get_template_part('loop/loop', 'blog-classic-sidebar');
	get_footer();				