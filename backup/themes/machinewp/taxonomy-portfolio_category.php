<?php 
	get_header(); 
	echo ebor_archive_header( get_option('portfolio_title', 'Our Works'), get_option('portfolio_header') );
	get_template_part('loop/loop-portfolio','grid');
	get_footer();				